/**
 * @file   main.cpp
 * @brief  Defines main-method
 * 
 *  
 * @author Stephan Seitz
 * @date   2015
 */


#include <iostream>
#include <stdlib.h>

#include <opencv2/opencv.hpp>
#include <opencv2/cudaobjdetect.hpp>
#include <boost/filesystem.hpp>

#include <string>
#include <limits>
#include <time.h>
#include <fstream>
// #include <opencv2/gpu/gpu.hpp>



#include "log.hpp"
#include "printinformation.hpp"
#include "filetools.hpp"
#include "hogsvmtrainer.hpp"
#include "imagetools.hpp"
#include "twostepcpuhog.hpp"

#include "twostepgpuhog.hpp"
#include "opencvtools.hpp"

using namespace cv;
using namespace cv::ml;
using namespace std;
using namespace boost::filesystem;



#define MARGIN_AROUND_PLATE 10

/**
 * @brief Class for parsing command line arguments
 * 
 */
struct Arguments {
    Arguments();
    void parse( int argc, char** argv );
    
    char* inputFileOrDirectory;
    bool bExecuteOnGpu;
	bool bTrainingMode;
	char* trainingCarImageDirectory;
	char* trainingPlatesOnlyDirectory;
	char* outputDirNegativeSamples;
	char* outputDirPositiveSamples;
};



/**
 * @brief Main method. Mainly, thought to test classes of this project
 */
int main(int argc, char **argv) 
{	
    printProgramInfo();
    
    cuda::setBufferPoolUsage(true);
    cuda::setBufferPoolConfig(0, 40000000,10);

    Arguments args;
    args.parse(argc, argv);


// 	Size winSize = cv::Size(120,30);
// 	Size blockSize = cv::Size(12,12);
// 	Size blockStride = cv::Size(6,6);
// 	Size cellSize = cv::Size(6,6);
	
    Size winStrideDetection = Size(4,4);
	
		
	Size winSize = cv::Size(120,40);
    Size blockSize = cv::Size(8,8);
    Size blockStride = cv::Size(4,4);
    Size cellSize = cv::Size(4,4);
	
	float jaccardCoefficient = 0.5f;
	
	double coarseThreshold = 0;
	
	string svmTrainingFile = "traingedSvm.yml";
	string svmDetectionFile = "../classifiers/brazilian4x4blockstride 2xboosting.yml";
	
	if(args.bTrainingMode) {
		_log("Generate negative samples. Output: %s", args.outputDirNegativeSamples);
// 		generateNegativeSamples(args.trainingCarImageDirectory, args.trainingPlatesOnlyDirectory, args.outputDirNegativeSamples, 0.5f, 20, 20);
		HogSvmTrainer trainer(args.trainingCarImageDirectory, args.trainingPlatesOnlyDirectory, jaccardCoefficient, winSize, blockStride, cellSize , blockSize, coarseThreshold);
		
		if(args.outputDirNegativeSamples){
			trainer.setNegativeOutputPath(args.outputDirNegativeSamples);
		}
		
		if(args.outputDirPositiveSamples) {
			trainer.setPositiveOutputPath(args.outputDirPositiveSamples);
		}
		trainer.setMarginAroundPlateX(MARGIN_AROUND_PLATE);
		trainer.setMarginAroundPlateY(MARGIN_AROUND_PLATE);
		trainer.setWindowStrideBoosting(Size(8,8));
		
// 		trainer.readSvm("good.yml");
// 		Mat imgS =imread("manyplates.jpg", CV_LOAD_IMAGE_GRAYSCALE);
// 		trainer.search(imgS);
		
		
		
		while(trainer.hasMoreFiles()){
// 			for(int i = 0; i < 10; i++){
			trainer.nextFile();
			Mat car = trainer.getCurrentCarImg();
			rectangle(car, trainer.getCurrentPlateRect(), Scalar(255,0,0));

// 			trainer.learnPositiveNearPlate(9, false);
			trainer.learnPositive();
			trainer.learnNegatives(10, false);
// 			trainer.learnNegatives(5, true, false);

			cout << trainer.getCurrentCarFile().filename() << endl;
// 			namedWindow("opencv", CV_WINDOW_AUTOSIZE);
// 			imshow("opencv", car);
// 			waitKey(0);
			
		}
// 		trainer.readModelFile("foo.xml1.mat");
		
		
		namedWindow("opencv", CV_WINDOW_AUTOSIZE);
// 		namedWindow("foo", CV_WINDOW_AUTOSIZE);
		for(int i = 0; i < 100; i++) {
			trainer.trainSvm();
// 			Mat imgS =imread("manyplates.jpg", CV_LOAD_IMAGE_GRAYSCALE);
// 			trainer.search(imgS);
			
			trainer.saveModelFile(svmTrainingFile + to_string(i) + ".mat");
			trainer.saveSvm(svmTrainingFile+ to_string(i));
			trainer.toFirstFile();
			while(trainer.hasMoreFiles()){
// 			for(int j = 0; j < 10; j++){
				char c = (char)cvWaitKey();
				if( c== 27) { // ESC key
					exit(EXIT_SUCCESS);
				}
				trainer.nextFile();
// 				trainer.search();
	// 			Mat car = trainer.getCurrentCarImg();
	// 			rectangle(car, trainer.getCurrentPlateRect(), Scalar(255,0,0));
				
				trainer.boostWithCurImage(0.3f);
// 				trainer.trainSvm();
	// 			trainer.learnNegatives(10, true, false);
				
				cout << trainer.getCurrentCarFile().filename() << endl;
				
			}
				
		}
		
		
		
		
		trainer.saveSvm(svmTrainingFile);
// 		cout << '\a'; // beep!
	} else {
		

		
		_log("Entering detection mode...");
		
		bool bVideoMode = false;
		VideoCapture vid;
		
		vector<string> inputFiles;
		if( is_directory(args.inputFileOrDirectory) ) {
			for ( directory_iterator it(path(args.inputFileOrDirectory)); it != directory_iterator(); it++ ) {
				if( isExistingImageFile(path(*it).string()) ) {
					inputFiles.push_back(path(*it).string());
					_log("Found image file\"%s\"", path(*it).c_str());
				}
			}
			if ( inputFiles.size() == 0 ) {
				_log("Couldn't find any images in %s", args.inputFileOrDirectory);
			}
		} else if ( isExistingImageFile(args.inputFileOrDirectory) ) {
			inputFiles.push_back(args.inputFileOrDirectory);
		} else if ( isExistingVideoFile(args.inputFileOrDirectory) ) {
			bVideoMode = true;
			vid = VideoCapture(args.inputFileOrDirectory);
		} else {
			_error ("Couldn't find \"%s\"! Terminate.", args.inputFileOrDirectory);
			exit ( EXIT_FAILURE );
		}
		
        _log("Load SVM form file: %s", svmDetectionFile );
        Ptr<ml::SVM> svm = ml::StatModel::load<ml::SVM>( svmDetectionFile );
		
//		_log("Setup cpu hog detector");
        HOGDescriptor cpu_hog;
        cpu_hog.winSize = winSize;
        cpu_hog.cellSize = cellSize;
        cpu_hog.blockSize = blockSize;
        cpu_hog.blockStride = blockStride;
        cpu_hog.setSVMDetector(get_svm_detector(svm));
        cpu_hog.nlevels = 6;
		
        Ptr<cuda::HOG> gpuHog = cuda::HOG::create(winSize,blockSize,blockStride,cellSize);
       gpuHog->setNumLevels(cpu_hog.nlevels);
       gpuHog->setHitThreshold(0);
       gpuHog->setWinStride(winStrideDetection);
       gpuHog->setScaleFactor(1.05);
       gpuHog->setGroupThreshold(0);
       gpuHog->setHitThreshold(0);
       vector<float> tmp;
       tmp = get_svm_detector(svm);
       gpuHog->setSVMDetector(tmp);
       gpuHog->setNumLevels(6);
		
         TwoStepCpuHog cpuPlateHog;
         cpuPlateHog.setBlockSize(blockSize);
         cpuPlateHog.setBlockStride(blockStride);
         cpuPlateHog.setWindowSize(winSize);
         cpuPlateHog.setCellSize(cellSize);

         cpuPlateHog.setWindowStride(winStrideDetection);
         cpuPlateHog.setSvmDetector(svm);

// 		 HybridPlateDetector hybridPlateHog;
//          hybridPlateHog.setBlockSize(blockSize);
//          hybridPlateHog.setBlockStride(blockStride);
//          hybridPlateHog.setWindowSize(winSize);
//          hybridPlateHog.setCellSize(cellSize);
// 
//          hybridPlateHog.setWindowStride(winStrideDetection);
//          hybridPlateHog.setSvmDetector(svm);


        _log("Setup my hog detector");
        TwoStepGpuHog plateHog;
         plateHog.setBlockSize(blockSize);
         plateHog.setBlockStride(blockStride);
         plateHog.setWindowSize(winSize);
         plateHog.setCellSize(cellSize);

         plateHog.setWindowStride(winStrideDetection);
         plateHog.setSvmDetector(svm);
		 
		time_t totalTimeMyMethod = 0;
		time_t totalTimeOcl = 0;
		
		uint totalTruePosMyMethod = 0;
		uint totalTruePosMaxMyMethod = 0;
		uint totalFalsePosMyMethod  = 0;
		uint totalMissedPlatesMyMethod = 0;
		float platePrecissionMyMethod = 0.0f;
		
		uint totalTruePosOcl = 0;
		uint totalTruePosMaxOcl = 0;
		uint totalFalsePosOcl  = 0;
		uint totalMissedPlatesOcl  = 0;
		float platePrecissionOcl = 0.0f;
		
// 		vector<Mat> imgVec;
// 		for ( string& curFile : inputFiles ) {
// 			cv::Mat inputImage;
// 			_log("Loading file \"%s\"", args.inputFileOrDirectory);
// 			inputImage = cv::imread(curFile, CV_LOAD_IMAGE_GRAYSCALE);
// 			imgVec.push_back(inputImage);
// 		}
// 		hybridPlateHog.detectImageSeries();
		
		if( bVideoMode ) {
			plateHog.detectRealTime(makePtr<VideoFrameSource>(&vid), true);
		} else {
			
            vector<Mat> inputImages;
            for ( string& curFile : inputFiles ) {
                cv::Mat inputImage;
                _log("Loading file \"%s\"", args.inputFileOrDirectory);
                inputImage = cv::imread(curFile, CV_LOAD_IMAGE_GRAYSCALE);
                if( inputImage.empty() ) {
                    _error("Could not read file \"%s\"", args.inputFileOrDirectory );
                } else {
                    inputImages.push_back(inputImage);
                }
            }
			
            vector<vector<Rect>> rects;
            vector<vector<double>> confs;

            time_t begin = clock();
            plateHog.detectRealTime(makePtr<MatVecFrameSource>(inputImages), true );
            time_t end = clock();

            double elapsed_secs_myMethod = double(end - begin) / CLOCKS_PER_SEC;

            cout << "my method: "<< elapsed_secs_myMethod << "s" << endl;
            cout << "my method: "<< inputFiles.size() / elapsed_secs_myMethod << "fps" << endl;
//            for ( string& curFile : inputFiles ) {
//                cv::Mat inputImage;
//                _log("Loading file \"%s\"", args.inputFileOrDirectory);
//                inputImage = cv::imread(curFile, CV_LOAD_IMAGE_GRAYSCALE);
//                if( inputImage.empty() ) {
//                    _error("Could not read file \"%s\"", args.inputFileOrDirectory );
//                } else {
//     //                resize(inputImage, inputImage, Size(cvRound(inputImage.cols / 0.6), cvRound(inputImage.rows / 0.6)) );

//                    // create copy of input image to test two methods and show results
//                    Mat cpyInput = inputImage.clone();
//                    Rect plateRect;

//                    path plateLocFile = path(curFile).replace_extension(".platelocation");
//                    _log("Try to find file with plate location (\"%s\")...", plateLocFile.c_str());
//                    if ( is_regular_file(plateLocFile) ) {
//                        ifstream ifs;
//                        ifs.open(plateLocFile.string());
//                        ifs >> plateRect.x;
//                        ifs >> plateRect.y;
//                        ifs >> plateRect.width;
//                        ifs >> plateRect.height;
//                        _log("Found plate %ix%i at (%i,%i)", plateRect.width, plateRect.height, plateRect.x, plateRect.y);
//                    } else {
//                        _log("Couldn't find \"%s\"", plateLocFile.c_str());
//                    }

//                    vector<Rect> plateRects;
//                    vector<double> plateWeights;

//                     cuda::GpuMat gpuImg;
//                     gpuImg.upload(inputImage);


//                     time_t begin_myMethod = clock();
////                      plateHog.detect(inputImage, plateRects, plateWeights);

//                     gpuHog->detectMultiScale(gpuImg, plateRects, &plateWeights);
//                    time_t end_myMethod = clock();


//     //                double sum = 0;
//     //                vector<float> cpuResults;
//     //                cpu_hog.compute(inputImage,cpuResults);
//     //                cuda::GpuMat gpuResults;
//     //                gpuHog->compute(gpuImg, gpuResults);
//     //                Mat foo;
//     //                gpuResults.download(foo);

//     ////                assert(cpuResults.size() == gpuResults.cols);
//     //                for(uint j = 0; j < gpuResults.rows; j++ ){
//     //                for(uint i = 0; i < gpuResults.cols; i++ ){
//     //                    sum +=(cpuResults[j+gpuResults.cols *i] - foo.at<float>(i,j));

//     //                    cout << sum /(j+gpuResults.cols *i) <<endl;
//     //                }
//     //                }
//     //                cout << sum/cpuResults.size() << endl;
//     //                exit(0);



//                    totalTimeMyMethod += end_myMethod - begin_myMethod;
//                    double elapsed_secs_myMethod = double(end_myMethod - begin_myMethod) / CLOCKS_PER_SEC;
//                    cout << "my method: "<< elapsed_secs_myMethod << "s" << endl;

//                     markResultsOfHogDetector(cpyInput, plateRects, plateWeights, true);



//                    _log("start detection");
//                    vector<Rect> found, found_filtered;
//                    vector<Point> foundPoints;

//                    vector<double> foundWeights;
//                    time_t begin = clock();
//     //                 cpuPlateHog.detect(inputImage, found, foundWeights);
//     //                cpu_hog.detectMultiScale(inputImage, found, foundWeights, 0, winStrideDetection, Size(), 1.05f, 0, false );
//                    time_t end = clock();
//                     markResultsOfHogDetector(inputImage, found, foundWeights, true);

//                    totalTimeOcl += end - begin;
//                    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//                    cout << "OCL method: " << elapsed_secs << "s" << endl;



//                    if ( plateRect.area() != 0 ) {

//                        plateRect.x -= MARGIN_AROUND_PLATE;
//                        plateRect.y -= MARGIN_AROUND_PLATE;
//                        plateRect.width += 2*MARGIN_AROUND_PLATE;
//                        plateRect.height += 2*MARGIN_AROUND_PLATE;

//                        uint truePos, falsePos;
//                        float jaccardOfMaximum;
//                        evaluateResults(plateRects, plateWeights, plateRect, 0.4f, truePos, falsePos, jaccardOfMaximum);
//                        _log("my method: %i true positive, %i false positive, plate precision: %f%%", truePos, falsePos, jaccardOfMaximum);
//                        totalTruePosMyMethod += truePos;
//                        totalFalsePosMyMethod += falsePos;
//                        platePrecissionMyMethod += jaccardOfMaximum;
//                        if ( jaccardOfMaximum >= jaccardCoefficient ) {
//                            totalTruePosMaxMyMethod++;
//                        }

//                        if( truePos == 0 ) {
//                            totalMissedPlatesMyMethod++;
//                        }

//                        evaluateResults(found, foundWeights, plateRect, 0.4f, truePos, falsePos, jaccardOfMaximum);
//                        _log("Opencv method: %i true positive, %i false positive, plate precision: %f%%", truePos, falsePos, jaccardOfMaximum);
//                        totalTruePosOcl += truePos;
//                        totalFalsePosOcl += falsePos;
//                        platePrecissionOcl += jaccardOfMaximum;

//                        if( truePos == 0 ) {
//                            totalMissedPlatesOcl++;
//                        }
//                        if ( jaccardOfMaximum >= jaccardCoefficient ) {
//                            totalTruePosMaxOcl++;
//                        }
//                    }

//                    rectangle(cpyInput, plateRect, Scalar(100,100,100), 3);
//                    namedWindow("my method", CV_WINDOW_AUTOSIZE);
//                    imshow("my method", cpyInput);

//     //				rectangle(inputImage, plateRect, Scalar(100,100,100), 3);
//     //				putText(inputImage, path(curFile).filename().string(), Point(50,50), 0, 1, Scalar(255,255,255));
//     //				namedWindow("opencv", CV_WINDOW_AUTOSIZE);
//     //				imshow("opencv", inputImage);


//     //                if ( 27 == waitKey(0) ) {
//     //                    exit ( EXIT_SUCCESS );
//     //                }


//                }
//            }

//            platePrecissionMyMethod /= inputFiles.size();
//            platePrecissionOcl /= inputFiles.size();

//            double totalSecondsMyMethod = ((double) totalTimeMyMethod) / CLOCKS_PER_SEC;
//            double totolSecondsOcl = ((double) totalTimeOcl) / CLOCKS_PER_SEC;
//            _log("Total time my method: %fs (total), %fs (per frame) => %f fps",  totalSecondsMyMethod, totalSecondsMyMethod / inputFiles.size(), inputFiles.size() / totalSecondsMyMethod);
//            _log("My method: precision %f%% (used jaccard %f as threshold), plate position accuracy %f",
//                 /* precison */ ((double) totalTruePosMyMethod ) / (totalTruePosMyMethod + totalFalsePosMyMethod) * 100.0,
//                            jaccardCoefficient,
//                 /* poscion accuracy of best result */ platePrecissionMyMethod);


//            _log("Total time OCL method: %fs (total), %fs (per frame) => %f fps",  totolSecondsOcl, totolSecondsOcl / inputFiles.size(), inputFiles.size() / totolSecondsOcl);
//            _log("OCL method: precision %f%% (used jaccard %f as threshold), plate position accuracy %f",
//                 /* precison */ ((double) totalTruePosOcl ) / (totalTruePosOcl + totalFalsePosOcl) * 100.0,
//                                jaccardCoefficient,
//                 /* poscion accuracy of best result */ platePrecissionOcl);
//     // 		} catch (cv::Exception& exc) {
//     // 			 exc.formatMessage();
//     // 		}
//     //		_log("Time fine search CPU: %fs", cpuPlateHog.getFineSearchTimePerRun());
//            _log("Time fine search GPU: %fs", plateHog.getFineSearchTimePerRun());
        }
	}
    return EXIT_SUCCESS;
}



Arguments::Arguments():
    inputFileOrDirectory(NULL),
    bExecuteOnGpu(false),
	bTrainingMode(false),
	trainingCarImageDirectory(NULL),
	trainingPlatesOnlyDirectory(NULL),
	outputDirNegativeSamples(NULL),
	outputDirPositiveSamples(NULL)
{
}

/**
 * @brief Parses command line arguments to member variables
 * 
 * @param argc argc of main()
 * @param argv argv of main()
 * @return void
 */
void Arguments::parse ( int argc, char** argv ) {
    if (argc == 1 ){
        printHelp();
        exit(EXIT_SUCCESS);
    } else {
        int lastArg = argc - 1;
        this->inputFileOrDirectory = argv[lastArg];
        
        for (int i = 1; i < lastArg; i++)
        {
            if (string(argv[i]) == "--gpu") {
                this->bExecuteOnGpu = true;
            } else if (string(argv[i]) == "--cpu") {
                this->bExecuteOnGpu = false;
            } else if (string(argv[i]) == "--train" || string(argv[i]) == "-t") {
				if (lastArg - i < 2) {
					_error("Please indicate folders for car images, plate only images and the output folder when using --train");
					printHelp();
					exit(EXIT_FAILURE);
				}
				
				this->bTrainingMode = true;
				this->trainingCarImageDirectory = argv[++i];
				errorAndExitIfNotADirectory(this->trainingCarImageDirectory);
				this->trainingPlatesOnlyDirectory = argv[++i];
				errorAndExitIfNotADirectory(this->trainingPlatesOnlyDirectory);
			} else if( string(argv[i]) == "--negout" ) {
				this->outputDirNegativeSamples = argv[++i];
				errorAndExitIfNotADirectory(this->outputDirNegativeSamples);
			} else if ( string(argv[i]) == "--posout" ) {
				this->outputDirPositiveSamples = argv[++i];
				errorAndExitIfNotADirectory(this->outputDirPositiveSamples);
			}
        }
    }
}