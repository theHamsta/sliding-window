This project implements the algorithm proposed in http://www.decom.ufop.br/pos/site_media/uploads_ppgcc/publicacao/prates2014.pdf  (also available in this repository as ./papers/prates2014)
used for automatic license plate recognition in motion data using Histogram of Oriented Gradients object detection. Unlike the source code of the author,
this code aims to have a good performance using CUDA-GPU-acceleration. Additionally, it also tries to take full advantage of the used OpenCV framework using always OpenCV functions if available.
The original inefficient code was never published as the code indicated in the publication is from a former project \cite pratesFirstPub which used a much simpler algorithm.


Requirements:
- CMake for building (version > 2.8)
- Boost (version > 1.36, components: boost_system, boost_filesystem, boost_serialization)
- OpenCV (version > 3.0)
- CUDA  (any version > 2.0 should do the job)
- doxygen for generation of documentation (optional)


Build instructions (documentation is also built into /doc if doxygen is installed):
  mkdir build
  cd build
  cmake ..
  make
  