#ifndef TWOSTEPCPUHOG_HPP
#define TWOSTEPCPUHOG_HPP

/**
 * @file   twostepcpuhog.hpp
 * @brief  Defines TwoStepCpuHog
 *  
 * @author Stephan Seitz
 * @date 2015
 */

#include "twostephog.hpp"

#include <string>



/**
 * @brief Implementation of TwoStepHog using CPU for both coarse and refinement search.
 *
 * Can be used to check the correctness of TwoStepGpuHog as they should deliver nearly the results. TwoStepCpuHog used cv::HOGDescriptor, TwoStepGpuHog a corresponding GPU version.
 * Due to small differences in their algorithms and round-of errors the results show small numerical differences.
 * 
 */
class TwoStepCpuHog: public TwoStepHog
{
public:
	
	TwoStepCpuHog();
	~TwoStepCpuHog();
	
	virtual void detect(cv::Mat& img, std::vector< cv::Rect >& foundRect, std::vector< double >& confValues);
	virtual void detectRealTime(std::vector< cv::Mat >& img, std::vector< std::vector< cv::Rect > >& foundRect, std::vector< std::vector< double > >& confValues, bool bShowResults = true);
	virtual void detectRealTime( cv::Ptr<FrameSource> source, bool bShowResults = true) { assert( false && "not implemented" ); }
	
	
	virtual void setSvmCoarseDectector(cv::Ptr< cv::ml::SVM >& svm);
	virtual void setSvmFineDectector(cv::Ptr< cv::ml::SVM >& svm);	
	
	// set parameters for coarse search
	virtual inline void setCoarseWindowSize( const cv::Size& winSize ) { m_hogCoarseSearch.winSize = winSize; }
	virtual inline void setCoarseBlockSize( const cv::Size& blockSize ) { m_hogCoarseSearch.blockSize = blockSize; }
	virtual inline void setCoarseBlockStride( const cv::Size& blockStride ) { m_hogCoarseSearch.blockStride = blockStride; }
	virtual inline void setCoarseCellSize( const cv::Size& cellSize ) { m_hogCoarseSearch.cellSize = cellSize; }
	virtual void setCoarseNumBins(const uint numBins) { m_hogCoarseSearch.nbins = numBins; }
	
	// set parameters for refinement search
	virtual inline void setFineWindowSize( const cv::Size& winSize ) { m_hogFineSearch.winSize = winSize; }
	virtual inline void setFineBlockSize( const cv::Size& blockSize ) { m_hogFineSearch.blockSize = blockSize; }
	virtual inline void setFineBlockStride( const cv::Size& blockStride ) { m_hogFineSearch.blockStride = blockStride; }
	virtual inline void setFineCellSize( const cv::Size& cellSize ) { m_hogFineSearch.cellSize = cellSize; }
    virtual void setFineNumBins(const uint numBins) { m_hogFineSearch.nbins = numBins; }
	
private:
	CV_PROP cv::HOGDescriptor m_hogCoarseSearch; ///< CPU-SVM for coarse search
	CV_PROP cv::HOGDescriptor m_hogFineSearch; ///< CPU-SVM for fine search


};


#endif
