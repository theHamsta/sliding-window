#ifndef TWOSTEPHOG_HPP
#define TWOSTEPHOG_HPP

/**
 * @file   twostephog.hpp
 * @brief  Defines pure-virtual class TwoStepHog.
 * 
 * 
 * 
 * @author Stephan Seitz
 * @date 2015
 */

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#include "framesource.hpp"


// TODO: numbins!
enum TypeOfSearch { SEARCH_TYPE_COARSE, SEARCH_TYPE_FINE };

/**
 * @brief Pure-abstract class for a two step HOG algorithm as suggested by
 * http://www.decom.ufop.br/pos/site_media/uploads_ppgcc/publicacao/prates2014.pdf designed for the recognition and precise localization of license plates.
 * 
 * The algorithm performs a coarse multi-scale search to determine the approximate position of the license plate followed by a fine multi-scale search in 
 * promising regions at their respective scale to refine the position and scale of the plate. Both steps can be either performed by CPU (implemented by TwoStepCpuHog) or
 * GPU (implemented by TwoStepGpuHog). Also the compute intensive coarse search can be could be done on GPU and the easier fine search on CPU as it was realized by
 * HybridPlateDetector which disappeared form the repo because it was no longer maintained (you can take a look on revision older than 8a31c1f: 29-05-2016).
 * 
 * The parameters as well as SVM used for coarse and fine search can be set separately using the functions setCoarseXXX/setFineXXX and setSvmCoarseDectector/setSvmFineDectector.
 */
class TwoStepHog {
public:
	TwoStepHog();
	~TwoStepHog();
	
	void setDefaultForBrazilianPlates();
	void setDefaultForGreekPlates();

	virtual void setSvmCoarseDectector( cv::Ptr< cv::ml::SVM >& svm ) = 0; ///< @brief Sets SVM detector for coarse search
	virtual void setSvmFineDectector( cv::Ptr< cv::ml::SVM >& svm ) = 0; ///< Sets SVM detector for fine search
	virtual void setSvmDetector( cv::Ptr< cv::ml::SVM >& svm ) { setSvmCoarseDectector(svm); setSvmFineDectector(svm); } ///< Sets SVM detector for both coarse and fine search
	
	/**
	 * @brief Performs a two-step sliding window search on multi-scales to find license plates in a image (coarse + refinement search)
	 * 
	 * @param img Image on which the search for license plates should be performed
	 * @param foundRect Detected plate rectangles
	 * @param confValues Confidence values of each detected rectangle
	 * @return void
	 */
	virtual void detect( cv::Mat& img, std::vector<cv::Rect>& foundRect, std::vector<double>& confValues ) = 0;
	
	/**
	 * @brief Finds license plates in a series of images. Via assembly processing this method can me more efficient than multiple calls of detect().
	 * 
	 * @param img Image on which the search for license plates should be performed
	 * @param foundRect Detected plate rectangles
	 * @param confValues Confidence values of each detected rectangle
	 * @param bShowResults Show results in real time in a window
	 * @return void
	 */
	virtual void detectRealTime( std::vector<cv::Mat>& img, std::vector<std::vector<cv::Rect>>& foundRect, std::vector<std::vector<double>>& confValues, bool bShowResults = true) = 0;
	
	/**
	 * @brief Finds license plates in a series of images. Via assembly processing this method can me more efficient than multiple calls of detect().
	 * 
	 * @param source FrameSource as a source of images on which a search for license plates should be performed
	 * @param bShowResults Show results in real time in a window
	 * @return void
	 */
	virtual void detectRealTime( cv::Ptr<FrameSource> source, bool bShowResults = true) = 0;
	
	inline void setCoarseWindowStride( const cv::Size& winStride ) { m_winStrideCoarse = winStride; }
	virtual void setCoarseWindowSize( const cv::Size& winSize ) = 0;
	virtual void setCoarseBlockSize( const cv::Size& blockSize ) = 0;
	virtual void setCoarseBlockStride( const cv::Size& blockStride ) = 0;
	virtual void setCoarseCellSize( const cv::Size& cellSize ) = 0;
	virtual void setCoarseNumBins( const uint numBins ) = 0;
	inline void setCoarseScales ( const std::vector<double>& scales ) { m_scalesCoarse = scales; }
	
	inline void setFineWindowStride( const cv::Size& winStride ) { m_winStrideFine = winStride; }
	virtual void setFineWindowSize( const cv::Size& winSize ) = 0;
	virtual void setFineBlockSize( const cv::Size& blockSize ) = 0;
	virtual void setFineBlockStride( const cv::Size& blockStride ) = 0;
	virtual void setFineCellSize( const cv::Size& cellSize ) = 0;
	virtual void setFineNumBins( const uint numBins ) = 0;
	inline void setFineScales ( const std::vector<double>& scales ) { m_scalesFine = scales; }
	
	inline void setWindowStride( const cv::Size& winStride ) { setCoarseWindowStride(winStride); setFineWindowStride(winStride); }
	inline void setWindowSize( const cv::Size& winSize ) { setCoarseWindowSize(winSize); setFineWindowSize(winSize); }
	inline void setBlockSize( const cv::Size& blockSize )  { setCoarseBlockSize(blockSize); setFineBlockSize(blockSize); }
	inline void setBlockStride( const cv::Size& blockStride ) { setCoarseBlockStride(blockStride); setFineBlockStride(blockStride); }
	inline void setCellSize( const cv::Size& cellSize ) { setCoarseCellSize(cellSize); setFineCellSize(cellSize); }
	inline void setNumBins( const uint numBins ) { setCoarseNumBins(numBins); setFineNumBins(numBins); }
	inline void setScales ( const std::vector<double>& scales ) { setCoarseScales(scales); setFineScales(scales); }
	
	
	/**
	 * @brief Sets scales on which the coarse should be performed on. The image will be scaled to the inverse of these scales (equivalent to a scaling of the window but more efficient for sliding window search)
	 * 
	 * @param numLevels Number of wished scales
	 * @param scaleIncrease Increase of factor at next level (factors will be: s0, s0 + scaleIncrease, s0 + 2*scaleIncrease, ...
	 * @param s0 Smallest window scale. Plates smaller than the window size scaled with this factor will not be recognized.
	 * @return void
	 */
	inline void fillCoarseScales( uint numLevels, double scaleIncrease, double s0 = 1.0 );
	 /**
	 * @brief Sets scales on which the refinement should be performed on. The image will be scaled to the inverse of these scales (equivalent to a scaling of the window but more efficient for sliding window search)
	 * 
	 * @param numLevels Number of wished scales
	 * @param scaleIncrease Increase of factor at next level (factors will be: s0, s0 + scaleIncrease, s0 + 2*scaleIncrease, ...
	 * @param s0 Smallest window scale. Plates smaller than the window size scaled with this factor will not be recognized.
	 * @return void
	 */
	inline void fillFineScales( uint numLevels, double scaleIncrease, double s0 = 1.0 );
		/**
	 * @brief Sets scales on which the coarse should be performed on. The image will be scaled to the inverse of these scales (equivalent to a scaling of the window but more efficient for sliding window search)
	 * @param scales Vector of scales on which the plate search should be preformed on
	 * @return void
	 */
	inline void setCoarseScales( std::vector<double>& scales ) { m_scalesCoarse = scales; }
	/**
	 * @brief Sets scales on which the refinement should be performed on. The image will be scaled to the inverse of these scales (equivalent to a scaling of the window but more efficient for sliding window search)
	 * @param scales Vector of scales on which the plate search should be preformed on
	 * @return void
	 */
	inline void setFineScales( std::vector<double>& scales ) { m_scalesFine = scales; }

	inline double getFineSearchTimePerRun() {
#ifndef NDEBUG
		return m_timeFineSearch / cv::getTickFrequency() / m_numFineSearches;
#else
		return -1.;
#endif	
	}
protected:	
	/**
 * @brief Performs a simple rectangle grouping (simplified replacement for the actually in the paper proposed non-maximum suppression)
 * 
 * @param rects Rectangles to be grouped
 * @param weights Confidence values of each detected rectangle (currently ignored)
 * @return void
 */
	static void nonMaximumSupression(std::vector<cv::Rect>& rects, std::vector<double>* weights = nullptr);
	/**
 * @brief Performs a simple rectangle grouping (simplified replacement for the actually in the paper proposed non-maximum suppression)
 * 
 * @param points Locations of found rectangles of the same size to be grouped
 * @param windowSize Size of the rectangles
 * @param[out] rects Grouped rectangles
 * @param weights Confidence values of each detected rectangle (currently ignored)
 * @return void
 */
	static void nonMaximumSupression(std::vector< cv::Point >& points, cv::Size windowSize, std::vector< cv::Rect >& rects,  std::vector< double >* weights = nullptr);
	
	// parameters for coarse search
	cv::Size m_winStrideCoarse; /// translation of each window in x- and y-direction in coarse sliding window search
	std::vector<double> m_scalesCoarse; /// scales on which the coarse search is performed
	double m_hitThresholdCoarse; /// hit threshold for coarse search (i.e. score of SVM over which a window is regarded as a detection)
	

	// parameters for fine search
	cv::Size m_winStrideFine; /// translation of each window in x- and y-direction in coarse sliding window search
	std::vector<double> m_scalesFine; /// scales on which the fine search is performed, with  respect to the scale of the detection in the coarse search
	double m_hitThresholdFine; /// hit threshold for fine search (i.e. score of SVM over which a window is regarded as a detection)

	uint m_marginAroundCoarseHit_x;
	uint m_marginAroundCoarseHit_y;
	
#ifndef NDEBUG
	uint64 m_timeFineSearch;
	uint m_numFineSearches;
	uint64 m_lastFineSearchStartTime;
#endif
	void startFineSearchCpuTimeMeasurement();
	void endFineSearchCpuTimeMeasurement(uint numFineSearches);

};

#endif