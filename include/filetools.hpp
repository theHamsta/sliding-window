#ifndef FILETOOLS_HPP
#define FILETOOLS_HPP

/**
 * @file   filetools.hpp
 * @brief  Provides various methods for file properties (currently only if a file is a (existing) video or image file)
 *  
 * @author Stephan Seitz
 * @date 2015
 */

#include <string>
#include <boost/filesystem.hpp>

/**
 * @brief Checks if file has one of the following file extensions: .mov, .avi, .mpg, .mpeg
 * 
 * @param file File to check
 * @return bool
 */
bool isVideoByExtension(boost::filesystem::path file);

/**
 * @brief Checks if file has one of the following file extensions: .mov, .avi, .mpg, .mpeg
 * 
 * @param file File to check
 * @return bool
 */
bool isVideoByExtension(std::string& file);

/**
 * @brief Checks if file has one of the following file extensions: .jpg, .jpeg, .bmp, .png
 * 
 * @param file File to check
 * @return bool
 */
bool isImageByExtension(boost::filesystem::path file);

/**
 * @brief Checks if file has one of the following file extensions: .jpg, .jpeg, .bmp, .png
 * 
 * @param file File to check
 * @return bool
 */
bool isImageByExtension(std::string& file);


/**
 * @brief Calls isVideoByExtension and checks if file is existing and a regular file
 * 
 * @param file File to check
 * @return bool
 */
bool isExistingVideoFile(boost::filesystem::path file);

/**
 * @brief Calls isVideoByExtension and checks if file is existing and a regular file
 * 
 * @param file File to check
 * @return bool
 */
bool isExistingVideoFile(std::string& file);

/**
 * @brief Calls isImageByExtension and checks if file is existing and a regular file
 * 
 * @param file File to check
 * @return bool
 */
bool isExistingImageFile(boost::filesystem::path file);

/**
 * @brief Calls isImageByExtension and checks if file is existing and a regular file
 * 
 * @param file File to check
 * @return bool
 */
bool isExistingImageFile(std::string& file);

#endif // FILETOOLS_HPP