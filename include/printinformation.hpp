#ifndef PRINTINFORMATION_HPP
#define PRINTINFORMATION_HPP

/**
 * @file   printinformation.hpp
 * @brief  Prints information and standard errors as used by main-method
 *  
 * @author Stephan Seitz
 * @date   2015
 */


#include "log.hpp"
#include "filetools.hpp"



void printProgramInfo();

void printHelp();

void errorInvalidArgs();

void errorAndExitIfNotADirectory(char* directory);

#endif // PRINTINFORMATION_HPP