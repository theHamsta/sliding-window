#ifndef LOG_HPP
#define LOG_HPP

/**
 * @file   log.hpp
 * @brief  Defines some useful logging macros
 * 
 * It was not checked whether they compile with other compilers than g++ (use of ##__VA_ARGS__ seems to be g++-specific), 
 * but the macros are not vital for program so that the printf can be easily removed in case of compile time errors.
 * The use of ##__VA_ARGS_ causes a annoying warning whenever ##__VA_ARGS__ is left empty (zero variadic arguments).
 * 
 * 
 * @author Stephan Seitz
 * @date 2015
 */

#include <stdio.h>
#include <string>

/// Prints a log-message without line number and file of the call
#define _logWithOutLine(MESSAGE, ...) printf("DEBUG: " MESSAGE "\n", ##__VA_ARGS__); fflush(stdout)
/// Prints a log-message with line number and file of the call
#define _log(MESSAGE, ...) printf("DEBUG: " MESSAGE " [%s:%i]\n", ##__VA_ARGS__, __FILE__, __LINE__); fflush(stdout)
/// Prints a error-message with line number and file of the call
#define _error(MESSAGE, ...) fprintf(stderr, "ERROR: " MESSAGE " [%s:%i]\n", ##__VA_ARGS__, __FILE__, __LINE__)


/**
 * @brief Helper class for macro LOG_CALL. Logs "Entering function [FUNCTION_NAME]" on creation and "Exiting function [FUNCTION_NAME]" on destruction
 * 
 */
struct FunctionLogger {
    std::string m_functionName;
    FunctionLogger(std::string fname_): m_functionName(fname_) { _logWithOutLine("Entering function %s", m_functionName.c_str()); }
    ~FunctionLogger() { _logWithOutLine("Exiting function %s", m_functionName.c_str()); }
};

/// Use this macro in a function call to log entering and leaving of the function body (very useful for debugging)
#define LOG_CALL FunctionLogger _token(std::string(__func__) + " [" + __FILE__ + ":" + std::to_string(__LINE__) + "]")

#endif // #ifndef LOG_HPP
