#ifndef HOGSVMTRAINER_HPP
#define HOGSVMTRAINER_HPP

/**
 * @file   hogsvmtrainer.hpp
 * @brief  Defines HogSvmTrainer, a class that generates a SVM-classifier for license plates from positive and negative samples
 *  
 * @author Stephan Seitz
 * @date 2015
 */

#include <opencv2/opencv.hpp>
// #include <opencv2/gpu/gpu.hpp> // gpu is currently not used for boosting (maybe better when using same detector for boosting and recognition?
#include <boost/filesystem.hpp>
#include <random>


/**
 * @brief Defines the categories positive and negative for samples
 * 
 */
enum SampleClass { POSITVE_SAMPLE, NEGATIVE_SAMPLE };

/**
 * @brief Generates a SVM-classifier for license plates for specific HOG parameters from positive and negative samples
 * 
 * HogSvmTrainer has two directories for generating the positive and negative samples.
 * m_carImgDir is a directory with images of a single car and m_plateImgDir contains the isolated license plates.
 * Images in m_plateImgDir need to have the same file names as their correspondences in m_carImgDir (including the file extension, 
 * batch-convert if necessary http://superuser.com/questions/71028/batch-converting-png-to-jpg-in-linux). 
 * The images in m_plateImgDir are only used to locate the license plate in m_carImgDir.
 * 
 * Positive samples can be generated from the current file in regions very near (defined by Jaccard index > m_jaccardLimit) to 
 * the plate location (learnPositiveNearPlate()) or with the exact plate image (learnPositive()).
 * 
 * Negative sample can be generated from the current file with learnNegatives(). Either in all regions of the image or relatively near the plate (0 < Jaccard index < m_jaccardLimit)
 * to lower the false positive rate and improve plate localization.
 * 
 * HogSvmTrainer has always a current image file that it is processing. nextFile() takes you to the next one if the class hasMoreFiles(). You can also go toFirstFile().
 * If you think you have enough samples, you can generate a SVM with the current samples (trainSvm()). After that, you can improve your hit rate
 * with Boosting (train the classifier with samples that are currently classified incorrectly). Boosting can lower the false positive rate and be used to generate additional negative samples boostWithCurImage(). The method uses
 * the SVM of the last call of trainSvm(). So remember to call it from time to time (e.g. after processing each image once).
 * 
 * Generated SVMs can be saved to a file (saveSvm(), saves also the used HOG parameters as comments into the file). It is recommended not to save only the resulting SVM as you might want others
 * to reproduce your results or to reuse samples that generated good SVMs for alternative HOG parameters. To save also the sub-images used to generate positive and negative set
 * m_posSamplesOutputDir and m_negSamplesOutputDir. Additionally, you can also save the feature vectors derived from these sub-images via saveModelFile(), which are 
 * unfortunately depended on the used HOG parameters. With these feature vectors, loadable via readModelFile(), you can restore exactly a former state of a HogSvmTrainer,
 * e.g. to get the data after a few rounds of successive boosting.
 * 
 * 
 * 
 */

class HogSvmTrainer
{
public:
//     HogSvmTrainer(char* carImageDir, char* plateImgDir, char* negSamplesOutputDir = NULL, float jaccardLimit = 0.5f , double hitThreshold = 1.0f);
	HogSvmTrainer(char* carImageDir, char* plateImgDir, float jaccardLimit, const cv::Size& windowSize, const cv::Size& blockStride, const cv::Size& cellSize, const cv::Size& blockSize, double hitThreshold);
    ~HogSvmTrainer();
    

	void toFirstFile();
	void nextFile();
	bool hasMoreFiles() const;

	// ways to generate positive samples
	void learnPositive();
	void learnPositivesFromDirectory( const std::string& path );
	void learnPositiveNearPlate( unsigned int numPositives );
	
	// ways to generate negative samples
	void learnNegatives( unsigned int numNegatives, bool bNearPlate );
	void learnNegativesFromDirectory( const std::string& path );
	void boostWithCurImage( float jaccardBoosting );
	
	void readSvm( const std::string& filename );
    void saveSvm( const std::string& filename );
	void trainSvm();


	void saveModelFile( const std::string& filename ) const;
	void readModelFile( const std::string& filename );
	
	void testSvmOnCurrentImg();
	void testSvmOnImg(cv::Mat& img);
	
    inline const cv::Mat getCurrentCarImg() const { return m_currentCarImg.clone(); }
    inline const boost::filesystem::path& getCurrentCarFile() const { return m_currentCarFile; }
    inline const cv::Mat getCurrentPlateImg() const { return m_currentPlateImg.clone(); }
    inline float getJaccardLimit() const { return m_jaccardLimit; }
    inline const cv::Rect& getCurrentPlateRect() { 
		if(!m_bPlateLocalized)
			localizePlate();
		return m_currentPlateRect; 
	}
	inline void setMarginAroundPlateX(const uint margin ) { m_marginAroundPlate_x = margin; }
	inline void setMarginAroundPlateY(const uint margin ) { m_marginAroundPlate_y = margin; }
	inline uint getMarginAroundPlateX(const uint margin ) const { return m_marginAroundPlate_x; }
	inline uint getMarginAroundPlateY(const uint margin ) const { return m_marginAroundPlate_y; }
	inline void setPositiveOutputPath(const boost::filesystem::path path) { m_posSamplesOutputDir = path; }
	inline void setNegativeOutputPath(const boost::filesystem::path path) { m_negSamplesOutputDir = path; }
	inline cv::Size getWindowStrideBoosting() const { return m_windowStrideBoosting; }
	inline void setWindowStrideBoosting(const cv::Size stride) { m_windowStrideBoosting = stride; }
    cv::Ptr<cv::ml::SVM> getSvm();

	inline void setNumBins( const int numBins ) { m_hog->nbins = numBins; }
	inline void setWinSigma( const int winSigma ) { m_hog->winSigma = winSigma; }
	inline void setL2HysThreshold( const int L2HysThreshold ) { m_hog->L2HysThreshold = L2HysThreshold; }
	inline void setGammaCorrection( const int gammaCorrection ) { m_hog->gammaCorrection = gammaCorrection; }
    
    inline void setJaccardLimit( float value ) {
        assert( value <= 1.0f && value >= 0.0f && "Jaccard coeeffcient must be between 0 and 1"); m_jaccardLimit = value; 
    }
    
private:
	void checkDirectories();
    void localizePlate();
	std::vector<float> extractFeatureVector(const cv::Mat& plateSizeImg);
	void learnSample( const cv::Mat& subimg , SampleClass sampleClass );
	void learnNegative( const cv::Mat& img );
	void learnPositive( const cv::Mat& img );
	void learnSamplesFromDirectory ( const std::string& path, SampleClass sampleClass );
	cv::Rect getRectNearPlate( SampleClass sampleClass );
	void initHog();	
	void printHogParams() const;
    
	boost::filesystem::path m_carImgDir; ///< Path to car images
    boost::filesystem::path m_plateImgDir; ///< Path to license plate images
    boost::filesystem::path m_negSamplesOutputDir; ///< Output directory for negative samples (no output if not initialized)
	boost::filesystem::path m_posSamplesOutputDir; ///< Output directory for positive samples (no output if not initialized)
    boost::filesystem::directory_iterator m_carDirIterator; ///< Points to current directory entry in m_carImgDir
    
    
    boost::filesystem::path m_currentCarFile; ///< path to current car image
    cv::Mat m_currentCarImg; ///< Current car image
    cv::Mat m_currentPlateImg; ///< Current plate image
	cv::Mat m_plateSizeMatrix; ///< Buffer with the size of the license plate to be used by class' functions to avoid frequent allocation and deallocation
    cv::Rect m_currentPlateRect; ///< Indicates (known) position and size of the plate in current car image.
	
	cv::Mat m_featureVectors; ///< List of feature vectors generated by the samples (generated by learnSample())
	cv::Mat m_labels;		///< List of class labels for m_featureVectors (positive +1 or negative -1)
    
    std::default_random_engine m_generator; ///< Random number generator for random sample generation (random subimage as positive or negative sample)
    
    float m_jaccardLimit; ///< Jaccard index to define whether a rectangle is considered a hit for a license plate
	double m_hitThreshold; ///< Threshold for SVM score that sufficient classify a sample as a license plate (positive class)
	cv::Size m_windowStrideBoosting; ///< Window stride (translation of window in x- and y-direction in sliding window search) when using boostWithCurImage()
	
	//HOG parameters (read HOG papers to know their exact meaning)
	cv::Size m_windowSize; ///< Window size for sliding window search and sample generation
	cv::Size m_cellSize; ///< Cell size for sliding window search and sample generation
	cv::Size m_blockSize; ///< Block size for sliding window search and sample generation
	cv::Size m_blockStride; ///< Block stride for sliding window search and sample generation
	
	int m_marginAroundPlate_x; ///< Optional horizontal margin around license plate for sample generation (e.g. include parts of the frame of the plate)
	int m_marginAroundPlate_y; ///< Optional vertical margin around license plate for sample generation (e.g. include parts of the frame of the plate)
	
	bool m_bPlateLocalized; ///< Is the plate already localized in the current car image? In the negative case localizePlate() is called if needed.
	bool m_bSvmTrained; ///< Is m_svm trained with the current samples in m_featureVectors? In the negative case trainSvm() is called if needed.
	
	uint m_numTrainedNegatives; ///< Number of negative samples in m_featureVectors
	uint m_numTrainedPositives; ///< Number of positive samples in m_featureVectors
	
	cv::Ptr<cv::ml::SVM> m_svm; ///< CPU SVM to be trained
	cv::Ptr<cv::HOGDescriptor> m_hog; ///< HOGDescriptor to calculate HOG features from subimages
	
	std::vector<std::string> m_foldersUsedForTraining; ///< Strings of folders used for training (information saved with the generated SVM file)
	
	
	uint m_numNegBoosting; ///< Number of negative samples generated by boostWithCurImage() (information saved with the generated SVM file)
	uint m_numNegNearPlate; ///< Number of negative samples that are generated from subimages near the plate location (information saved with the generated SVM file)
	uint m_numNegDistantToPlate; ///< Number of negative samples that are generated from subimages not near the plate location (information saved with the generated SVM file)
	uint m_numPosExactPlate; ///< Number of positive samples that are generated from subimages of the exact plate location (information saved with the generated SVM file)
	uint m_numPosNearPlate;///< Number of positive samples that are generated from subimages of near to the exact plate location (information saved with the generated SVM file)
 	uint m_numPosBoosting; ///< Number of positive samples that are generated by boostWithCurImage() (currently not used, should be always 0)
};




#endif // HOGSVMTRAINER_HPP