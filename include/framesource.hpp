#ifndef FRAMESOURCE_HPP
#define FRAMESOURCE_HPP

/**
 * @file   framesource.hpp
 * @brief  Defines classes that can provide frames (FrameSource, VideoFrameSource, MatVecFrameSource)
 * 
 * @author Stephan Seitz
 * @date   2015
 */

#include <opencv2/opencv.hpp>
#include <vector>
#include <assert.h>

#include "log.hpp"

/**
 * @brief Pure-virtual class that provides single frames from a (finite) image source
 * 
 */
class FrameSource
{
public:
	virtual bool hasMoreFrames() = 0;
	virtual uint getNumberOfFrames() = 0; 
	virtual void loadFrameIntoMat(cv::Mat& mat) = 0;
};

/**
 * @brief Extracts single frames from a video using OpenCV
 * 
 */
class VideoFrameSource : public FrameSource
{
public:

	VideoFrameSource( cv::VideoCapture* vid): m_vid(vid) {}
	inline bool hasMoreFrames() { return m_vid->isOpened(); }
	inline uint getNumberOfFrames() { return m_vid->get(CV_CAP_PROP_FRAME_COUNT); }
	inline void loadFrameIntoMat(cv::Mat& mat) {   
		assert(m_vid->isOpened());
        *m_vid >> m_colorImgBuffer;

        if(mat.type() != CV_8UC1) {
            mat =  cv::Mat(m_colorImgBuffer.size(), CV_8UC1);
        }


        cv::cvtColor(m_colorImgBuffer, mat, CV_BGR2GRAY);
        assert(mat.type() == CV_8UC1); }
	
private:
	cv::VideoCapture* m_vid; ///< cv::VideoCapture which can decode video images or videos from cameras
	cv::Mat m_colorImgBuffer; ///< intermediate colored result to be converted later in a grayscale version
};

/**
 * @brief Provides single frames from std::vector<cv::Mat> (vector of images)
 * 
 */
class MatVecFrameSource : public FrameSource
{
public:
    MatVecFrameSource(const std::vector<cv::Mat>& vec): m_frames(vec) {}
	inline bool hasMoreFrames() { return m_curFrameIdx < m_frames.size(); }
	inline uint getNumberOfFrames() { return m_frames.size(); }
    inline void loadFrameIntoMat(cv::Mat& mat) { assert(this->hasMoreFrames());  mat = m_frames[m_curFrameIdx];  m_curFrameIdx++; }
	
private:
	uint m_curFrameIdx; ///< index of current frame to know current frame in m_frames
	std::vector<cv::Mat> m_frames; ///< std::vector of cv::Mat as a collection of grayscale images
};



// //Should provide single frames from a folder of images. Was not finished
// class ImageFolderFrameSource : public FrameSource
// {
// 	ImageFolderFrameSource(std::string& dir {}
// 	inline bool hasMoreFrames() { }
// 	virtual void loadFrameIntoMat(cv::Mat& mat) {  }
// 	
// private:
// 
// };





#endif // FRAMESOURCE_HPP
