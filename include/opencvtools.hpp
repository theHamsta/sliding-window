#ifndef OPENCVTOOLS_HPP
#define OPENCVTOOLS_HPP

/**
 * @file   opencvtools.hpp
 * @brief  Provides various complementary functions for OpenCV 
 *  
 * @author Stephan Seitz
 * @date   2015
 */

#include <opencv2/opencv.hpp>
#include <algorithm>
#include <vector>

#define HOG_NO_PADDING Size()


/**
 * @brief Converts SVM from OpenCV format to decision hyperplane vector as expected by cv::HOGDescriptor
 * 
 * @param svm source cv::ml::SVM
 * @param hog_detector [out] vector<float> of the decision hyperplane defined by svm
 * @return void
 */
void get_svm_detector(const cv::Ptr<cv::ml::SVM>& svm, std::vector< float > & hog_detector );

/**
 * @brief Converts SVM from OpenCV format to decision hyperplane vector as expected by cv::HOGDescriptor
 * 
 * @param svm Source cv::ml::SVM
 * @return vector<float> of the decision hyperplane defined by svm
 */
std::vector< float > get_svm_detector( const cv::Ptr<cv::ml::SVM>& svm );

// TODO: more efficient version
/**
 * @brief Calculates the Jaccard coefficient of to rectangles (see https://en.wikipedia.org/wiki/Jaccard_index)
 * 
 * @param rect1 First rectangle
 * @param rect2 Second rectangle
 * @return Resulting Jaccard index
 */
float calculateJaccard(const cv::Rect& rect1, const cv::Rect& rect2);

/**
 * @brief Crops rectangle to image size (i.e. removes parts of the rectangle that are not within the given image size)
 * 
 * @param rect Rectangle to be cropped
 * @param imageSize Size of an image
 * @return void
 */
static inline void cropRect(cv::Rect& rect, const cv::Size imageSize)
{
	rect.x = std::max(0, rect.x);
	rect.y = std::max(0, rect.y);
	rect.width = std::min(imageSize.width - rect.x,  rect.width );
	rect.height = std::min(imageSize.height - rect.y,  rect.height);
	
}

/**
 * @brief Calculates number of true and false positives
 * 
 * @param hits Rectangles classified as hits
 * @param trueRect The true position of the license plate (problematic if there is more one)
 * @param jaccardThreshold Jaccard limit to distinguish hit and miss 
 * @param [out] truePos number of correctly detected plates
 * @param [out] falsePos  number of wrongly detected plates
 * @return void
 */

void evaluateResults( std::vector<cv::Rect> hits, cv::Rect trueRect, float jaccardThreshold, uint& truePos, uint& falsePos );

/**
 * @brief Calculates number of true and false positives and the Jaccard index of the hit with highest confidence (i.e. quality of estimated plate position)
 * 
 * @param hits Rectangles classified as hits
 * @param confVals Confidence values of hits
 * @param trueRect The true position of the license plate (problematic if there is more one)
 * @param jaccardThreshold Jaccard limit to distinguish hit and miss 
 * @param [out] truePos  number of correctly detected plates
 * @param [out] falsePos number of wrongly detected plates
 * @param [out] jaccardOfMaximum Jaccard index of hit with highest confidence
 * @return void
 */
void evaluateResults( std::vector<cv::Rect> hits, std::vector<double> confVals, cv::Rect trueRect, float jaccardThreshold, uint& truePos, uint& falsePos, float& jaccardOfMaximum );


// Scaling of OpenCV structures

static inline cv::Point operator *(double scalar, cv::Point point) { return cv::Point(cvRound(scalar*point.x), cvRound(scalar*point.y)); }
static inline cv::Point operator *(cv::Point point, double scalar ) { return cv::Point(cvRound(scalar*point.x), cvRound(scalar*point.y)); }

static inline cv::Size operator *(double scalar, cv::Size size) { return cv::Size(cvRound(scalar*size.width), cvRound(scalar*size.height)); }
static inline cv::Size operator *(cv::Size size, double scalar ) { return cv::Size(cvRound(scalar*size.width), cvRound(scalar*size.height)); }

static inline cv::Rect operator *(double scalar, cv::Rect rect) { return cv::Rect(scalar*rect.tl(), scalar*rect.size()); }
static inline cv::Rect operator *(cv::Rect rect, double scalar ) { return cv::Rect(scalar*rect.tl(), scalar*rect.size()); }


/**
 * @brief Pointwise addition of a point plus an offset
 * 
 * @param point 
 * @param offset 
 * @return cv::Size
 */
static inline cv::Size operator +(cv::Point point, cv::Point offset) { return cv::Point( point.x + offset.x, point.y + offset.y ); }



#endif
