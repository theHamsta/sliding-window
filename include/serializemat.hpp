#ifndef SERIALIZEMAT_HPP
#define SERIALIZEMAT_HPP

#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/vector.hpp>


/**
 * @file   serializemat.hpp
 * @brief  Code to serialize cv::Mat using boost::serialization
 *
 * When trying to serialize Mat using the boost::serialization a compile time error is thrown due to some missing methods 
 * which are provided by this file using code from http://stackoverflow.com/questions/4170745/serializing-opencv-mat-vec3f
 * 
 * @author Christoph Heindl (adapted), http://stackoverflow.com/questions/4170745/serializing-opencv-mat-vec3f
 * @date 2015
 */


// Parted copied from http://stackoverflow.com/questions/4170745/serializing-opencv-mat-vec3f

BOOST_SERIALIZATION_SPLIT_FREE(cv::Mat)

namespace boost {
namespace serialization {

    /*** Mat ***/
    template<class Archive>
    void save(Archive & ar, const cv::Mat& m, const unsigned int version)
    {
      size_t elemSize = m.elemSize(), elemType = m.type();

      ar & m.cols;
      ar & m.rows;
      ar & elemSize;
      ar & elemType; // element type.
      size_t dataSize = m.cols * m.rows * m.elemSize();

      //cout << "Writing matrix data rows, cols, elemSize, type, datasize: (" << m.rows << "," << m.cols << "," << m.elemSize() << "," << m.type() << "," << dataSize << ")" << endl;

      for (size_t dc = 0; dc < dataSize; ++dc) {
          ar & m.data[dc];
      }
    }

    template<class Archive>
    void load(Archive & ar, cv::Mat& m, const unsigned int version)
    {
        int cols, rows;
        size_t elemSize, elemType;

        ar & cols;
        ar & rows;
        ar & elemSize;
        ar & elemType;

        m.create(rows, cols, elemType);
        size_t dataSize = m.cols * m.rows * elemSize;

        //cout << "reading matrix data rows, cols, elemSize, type, datasize: (" << m.rows << "," << m.cols << "," << m.elemSize() << "," << m.type() << "," << dataSize << ")" << endl;

        for (size_t dc = 0; dc < dataSize; ++dc) {
                  ar & m.data[dc];
        }
    }

}
}

// Own methods:

/**
 * @brief Serializes (saves) cv::Mat to file
 * 
 * @param m Matrix to serialize
 * @param filename String with relative/absolute path to destination file
 * @return void
 */
void saveMat(cv::Mat& m, std::string filename);

/**
 * @brief Loads cv::Mat from file
 * 
 * @param m Variable to be filled with files saved cv::Mat
 * @param filename String with relative/absolute path to source file
 * @return void
 */
void loadMat(cv::Mat& m, std::string filename);

#endif