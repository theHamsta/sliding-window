#ifndef IMAGETOOLS_HPP
#define IMAGETOOLS_HPP

/**
 * @file   imagetools.hpp
 * @brief  Provides simply image processing routines
 *  
 * @author Stephan Seitz
 * @date   2015
 */


#include <opencv2/opencv.hpp>



cv::Mat getGrayChannel(const cv::Mat& imgRGB);

CvPoint findBestMatch( const cv::Mat& img, const cv::Mat& subImg);

/**
 * @brief Visualizes results of a HOG detector. Overloaded version that additionally highlights the hit with highest confidence
 * 
 * @param img Car image
 * @param foundRects Found rectangles
 * @param foundWeights Confidence values for each hit
 * @param bTextOutput  Give a nice text feedback on found hits?
 * @param primaryColor Primary color to mark strongest hit
 * @param secondaryColor Secondary color mark all other hits
 * @return void
 */
void markResultsOfHogDetector(cv::Mat& img, const std::vector< cv::Rect >& foundRects, const std::vector< double >& foundWeights, bool bTextOutput = false, const cv::Scalar& primaryColor = cv::Scalar(255,0,0), const cv::Scalar& secondaryColor = cv::Scalar(0,255,0));


/**
 * @brief Visualizes results of a HOG detector
 * 
 * @param img Car image
 * @param foundRects  Found rectangles
 * @param bTextOutput Give some nice text feedback on results
 * @param primaryColor Color to mark hits
 * @return void
 */
void markResultsOfHogDetector(cv::Mat& img, const std::vector< cv::Rect >& foundRects, bool bTextOutput = false, const cv::Scalar& primaryColor = cv::Scalar(255,0,0));


#endif // IMAGETOOLS_HPP