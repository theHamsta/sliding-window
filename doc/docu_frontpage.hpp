/**
 * @file   docu_frontpage.hpp
 * @brief  Defines the front page of the doxygen generated documentation 
 *  
 * @author Stephan Seitz
 * @date 2015
 */

/*! \mainpage Overview
 *
\section intro_sec Introduction

This project implements the algorithm proposed in \cite prates2014  (also available in this repository as ./papers/prates2014)
used for automatic license plate recognition in motion data using Histogram of Oriented Gradients object detection. Unlike the source code of the author,
this code aims to have a good performance using CUDA-GPU-acceleration. Additionally, it also tries to take full advantage of the used OpenCV framework using always OpenCV functions if available.
It is important to mention that the original inefficient code was never published as the code indicated in the publication is from a former project \cite pratesFirstPub which used a much simpler algorithm.

Histograms of Oriented Gradients (HOG) provide a powerful feature for object detection (see \cite dalal2005). Objects are usually detected by performing a sliding window search: Windows are
moved over the image, the HOG-features are calculated for each window and handed over to classifier to decide whether this window contains the wished object or not. To detect objects of various sizes this search
is performed on various scales (e.g. the original image is scaled). Usually, only a non-adaptive one-step search is done (see original algorithm for license plates in \cite pratesFirstPub). This algorithm was
extended later to an adaptive two-step algorithm which uses less windows and scales in a first coarse search and refines that inexact position and sizes of the rectangles later in a second refinement search, 
as described in \cite prates2014.

You may also take a look at \cite fastGpuHog which is a project similar to this one, but with an one-step search.

See ./presentation/presentation.pdf for the slides of the final presentation of the project held on 28/12/15.

\section doc_sec Documentation of the Project
\subsection dep_sec Dependencies and Installation

The following dependencies of the project should be provided:
- CMake for building (version > 2.8)
- Boost (version > 1.36, components: boost_system, boost_filesystem, boost_serialization)
- OpenCV (version > 3.0)
- CUDA (any version > 2.0 should do the job)
- doxygen for automatic generation of documentation (optional)

Build instructions (documentation is also built in /doc if doxygen is installed):
\code
  mkdir build
  cd build
  cmake ..
  make
\endcode

\subsection gen_training_data Automatic Generation of Training Data

A down side of the original CPU project by Prates \cite prates2014 was that the generation of positive and negative samples for the training of SVMs was not fully automatized.
This made it difficult to reproduce results or generate quickly different classifiers for different HOG parameters, with different properties or for different data bases. Additionally,
used SVMs were not in the OpenCV format nor included in the project to test it after downloading.

This project provides a class HogSvmTrainer, which can be used for automatic generation of training samples and trained SVMs. See HogSvmTrainer for detailed description.

You can download two data bases for Greek and Brazilian license plates from http://www.decom.ufop.br/menotti/imgs-vlpl/
(in this case you need to convert license plate image files to the same format as the car image files.
Batch-convert like it's explained here: http://superuser.com/questions/71028/batch-converting-png-to-jpg-in-linux).

\subsection detection Detection of License Plates using Standard OpenCV Library

Without any modifications, the project uses cv::cuda::HOG in TwoStepGpuHog for two-step GPU detection. This works quite fine
as it was expected from the project to use as much OpenCV code as possible. Using the integration into a general purpose framework for
a specific application might lower the performance but make it easier to maintain the code and use future improvement of the framework.

\subsection detection_custom Detection of License Plates Using a Custom OpenCV Library

Another goal of the project was to achieve optimal performance with data-independent optimizations to process video data in real time (without tracking of already found plates in previous frames).
Therefore, the OpenCV-libraries were analysed for potential optimizations as they contain nearly all the critical code. The performance and the time share of the single
processing steps highly depends on the parameters used for HOG multi-scale search. But the most critical step is the SVM-classification in which a scalar product is executed 
(kernel compute_confidence_hists_kernel_many_blocks in opencv/modules/cudaobjdetect/source/cuda/hog.cu) with a time share of typically 85% of the hole execution time. For this reason,
this kernel offers the highest potential for optimization. The kernel is really simple and already pretty much optimized. But a few issues have been observed:

- All CUDA calls are blocking, synchronous and use the default stream (prevents CPU from working at the same time and generates overhead due to synchronization and data exchange with CPU)
- Resources are allocated and deallocated at each call
- The parameters for the SVM as well as the HOG parameters are transferred at each call to the GPU though they cannot be changed after initialization
- Aforementioned data transferred are blocking and synchronous which causes gaps in GPU kernel execution time line
- The scalar product uses single precision. Maybe the new half precision calculations which crunch the double amount of data have sufficient precision (could be changed easily in the kernel)

The first four issues could be easily fixed without changing the actual kernel by the introduction of an asynchronous HOG class which can be integrated into the project when the
folder opencv/modules/cudaobjdetect by a patched version in this repository (cudaobjdetect.tar.gz). To use the new class cv::cuda::AsychronousHog, the sliding window project has to be
compiled with the flag CUSTOM_CV. This will give you a small performance improvement. The old class cv::cuda::HOG will be still available.


\section results Results and Outlook

In this project, a flexible and high-performance version of the algorithm proposed in \cite prates2014 has been developed. It is easily possible to generate new classifiers and samples from and for
a given database. The original OpenCV libraries provide efficient GPU methods which are usually sufficient for an acceptable performance. Small changes in the OpenCV can slightly improve the performance
but this would require a separate class that needs to be justified and explained to users. This would collide with the OpenCV-philosophy of keeping things simple and easy to use.
However, the minor change that prevents multiple data transfers of SVM-coefficients and HOG-parameters could be easily integrated into the OpenCV-library.

If performance is a crucial point in an application, a separate library based on the OpenCV-version is recommended to be developed which could be more specialized to current application. Nevertheless,
the algorithm is numerically limited by the evaluation of the SVMs scalar product. For that, the only way substantially improve performance is a good parameter tuning and good SVM classifiers to
drastically reduce the amount windows to classify. This was not the focus of this project, but an improvement of the implementation via general-purpose GPU computing using a already existing framework.
With possibility to automatically generate SVMs it should be possible to find a perfect tuning for the HOG parameter as well as two good SVMs for coarse and refinement search (parameters and SVMs can
now be set separately for coarse and refinement search).

 */