#include "twostephog.hpp"

#include <algorithm>
#include <assert.h>

#include "opencvtools.hpp"
#include "log.hpp"

using namespace std;
using namespace cv;

#define  DEFAULT_MARGIN_AROUND_COARSE_HIT_X 15
#define  DEFAULT_MARGIN_AROUND_COARSE_HIT_Y 15
	
#define  DEFAULT_HIT_THRESHOLD_COARSE 0
#define  DEFAULT_HIT_THRESHOLD_FINE 0

TwoStepHog::TwoStepHog():
	m_hitThresholdCoarse(DEFAULT_HIT_THRESHOLD_COARSE),
	m_hitThresholdFine(DEFAULT_HIT_THRESHOLD_FINE),
	m_timeFineSearch(0),
	m_numFineSearches(0)
{
    fillCoarseScales(6, 0.2, 0.6);
	fillFineScales(4, 0.05);
	
	m_marginAroundCoarseHit_x = DEFAULT_MARGIN_AROUND_COARSE_HIT_X;
	m_marginAroundCoarseHit_y = DEFAULT_MARGIN_AROUND_COARSE_HIT_Y;


}



TwoStepHog::~TwoStepHog()
{

}


void TwoStepHog::setDefaultForBrazilianPlates()
{
	Size winSize = cv::Size(120,40);
	Size blockSize = cv::Size(8,8);
	Size blockStride = cv::Size(8,8);
	Size cellSize = cv::Size(4,4);
	
	setCoarseWindowSize(winSize);
	setCoarseBlockSize(blockSize);
	setCoarseBlockStride(blockStride);
	setCoarseCellSize(cellSize);
	
	setFineWindowSize(winSize);
	setFineBlockSize(blockSize);
	setFineBlockStride(blockStride);
	setFineCellSize(cellSize);
}

void TwoStepHog::setDefaultForGreekPlates()
{
	assert( false && "not implemented" );
}


void TwoStepHog::fillCoarseScales(uint numLevels, double scaleIncrease, double s0)
{
	m_scalesCoarse.clear();
	double s = s0;
	for( uint i = 0; i < numLevels; i++ ) {
		_log("Set coarse scale %f", s);
		m_scalesCoarse.push_back(s);
		s += scaleIncrease;
	}
}

void TwoStepHog::fillFineScales(uint numLevels, double scaleIncrease, double s0)
{
	m_scalesFine.clear();
	double s = s0;
	for( uint i = 0; i < numLevels; i++ ) {
		m_scalesFine.push_back(s);
		_log("Set fine scale %f", s);
		s += scaleIncrease;
	}
}


void TwoStepHog::startFineSearchCpuTimeMeasurement()
{
#ifndef NDEBUG
	m_lastFineSearchStartTime = getCPUTickCount();
#endif
}

void TwoStepHog::endFineSearchCpuTimeMeasurement(uint numFineSearches)
{
#ifndef NDEBUG
	uint64 endTime = getCPUTickCount();
	m_timeFineSearch += endTime - m_lastFineSearchStartTime;
	m_numFineSearches += numFineSearches;
#endif
}


void TwoStepHog::nonMaximumSupression(vector< Rect >& rects, vector< double >* weights)
{
		// idea from http://stackoverflow.com/questions/21421070/opencv-grouprectangles-getting-grouped-and-ungrouped-rectangles
    int size = rects.size();
    for( int i = 0; i < size; i++ )
    {
		// add each rectangle another time to the list, to prevent singular rectangles to be extinguished
        rects.push_back(rects[i]);
    }
    groupRectangles(rects, 1, 0.2);
}


void TwoStepHog::nonMaximumSupression(vector< Point >& points, Size windowSize, vector< Rect >& rects, vector< double >* weights )
{
	rects.clear();
	for( Point p : points ) {
		rects.push_back(Rect(p, windowSize));
	}
	
	// idea from http://stackoverflow.com/questions/21421070/opencv-grouprectangles-getting-grouped-and-ungrouped-rectangles
    int size = rects.size();
    for( int i = 0; i < size; i++ )
    {
        rects.push_back(rects[i]);
    }
    groupRectangles(rects, 1, 1.8);
}





