#include "serializemat.hpp"



using namespace cv;
using namespace std;

void saveMat(Mat& m, string filename) {
		ofstream ofs(filename.c_str());
		boost::archive::binary_oarchive oa(ofs);
		//boost::archive::text_oarchive oa(ofs);
		oa << m;
}

void loadMat(Mat& m, string filename) {
		std::ifstream ifs(filename.c_str());
		boost::archive::binary_iarchive ia(ifs);
		//boost::archive::text_iarchive ia(ifs);
		ia >> m;
}