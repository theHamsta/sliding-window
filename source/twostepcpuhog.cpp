#include "twostepcpuhog.hpp"
#include "opencvtools.hpp"


using namespace std;
using namespace cv;


TwoStepCpuHog::TwoStepCpuHog():
	TwoStepHog()
{
}

TwoStepCpuHog::~TwoStepCpuHog()
{

}

void TwoStepCpuHog::detect(Mat& img, vector< Rect >& foundRect, vector< double >& confValues)
{
	
	Mat scaledImg;
	Mat subImg; 
	Mat scaledSubImg;
	
	for ( double curScaleCoarse : m_scalesCoarse ) {
		
		Size localSearchSize( m_hogCoarseSearch.winSize.width + 2 * m_marginAroundCoarseHit_x, m_hogCoarseSearch.winSize.height + 2 * m_marginAroundCoarseHit_y );
		
		if( curScaleCoarse == 1.0 ) {
			scaledImg = img;
		} else {
			Size scaledSize = Size(
				cvRound( img.cols / curScaleCoarse ),
				cvRound( img.rows / curScaleCoarse )
			);
			resize(img, scaledImg, scaledSize);
		}
		
		vector<Point> foundLocationsCoarse;
		vector<double> foundWeightsCoarse;

		m_hogCoarseSearch.detect(scaledImg, foundLocationsCoarse, foundWeightsCoarse, m_hitThresholdCoarse, m_winStrideCoarse, HOG_NO_PADDING);
		
		for( Point& curPoint : foundLocationsCoarse ) {
			foundRect.push_back(Rect( curScaleCoarse * curPoint, curScaleCoarse * m_hogCoarseSearch.winSize));
		}
		confValues.insert(confValues.end(), foundWeightsCoarse.begin(), foundWeightsCoarse.end());

		startFineSearchCpuTimeMeasurement();
        for ( uint j = 0; j < foundLocationsCoarse.size(); j++ ) {
            Rect localSearchArea( Point(foundLocationsCoarse[j].x - m_marginAroundCoarseHit_x, foundLocationsCoarse[j].y - m_marginAroundCoarseHit_y), localSearchSize );
            cropRect(localSearchArea, Size(scaledImg.cols, scaledImg.rows));
			
            subImg = scaledImg(localSearchArea);
			
            for ( double curScaleFine : m_scalesFine ) {
                if ( curScaleFine == 1.0 ) {
                    scaledSubImg = subImg;
                } else {
                    Size scaledSubSize = Size(
                        cvRound( subImg.cols / curScaleFine ),
                        cvRound( subImg.rows / curScaleFine )
                    );
                    resize(subImg, scaledSubImg, scaledSubSize);
                }
				
                vector<Point> foundLocationsFine;
                vector<double> foundWeightsFine;
				
                m_hogFineSearch.detect(scaledSubImg, foundLocationsFine, foundWeightsFine, m_hitThresholdFine,  HOG_NO_PADDING);
// 				Size scaledWinSize( m_hogFineSearch.winSize.width / curScaleCoarse, m_hogFineSearch.winSize.height / curScaleCoarse );
                for ( Point& curPoint : foundLocationsFine ) {
                    foundRect.push_back(Rect(curScaleCoarse * ((curScaleFine * curPoint) + localSearchArea.tl()), m_hogFineSearch.winSize * curScaleCoarse * curScaleFine));
                }
                confValues.insert(confValues.end(), foundWeightsFine.begin(), foundWeightsFine.end());
            }
        }
		endFineSearchCpuTimeMeasurement(foundLocationsCoarse.size());
    }
	
	// TODO: Non-Maximum suppression
	

}


void TwoStepCpuHog::setSvmCoarseDectector(cv::Ptr< cv::ml::SVM >& svm)
{
	m_hogCoarseSearch.setSVMDetector(get_svm_detector(svm));
}

void TwoStepCpuHog::setSvmFineDectector(Ptr<cv::ml::SVM>& svm)
{
	m_hogFineSearch.setSVMDetector(get_svm_detector(svm));
}

void TwoStepCpuHog::detectRealTime(vector< Mat >& img, vector< vector< Rect > >& foundRect, vector< vector< double > >& confValues, bool bShowResults)
{
	assert( false && "not implemented!" );
}












