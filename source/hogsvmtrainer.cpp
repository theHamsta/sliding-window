#include "hogsvmtrainer.hpp"

#include <assert.h>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <opencv2/opencv.hpp>



#include "imagetools.hpp"
#include "filetools.hpp"
#include "log.hpp"
#include "serializemat.hpp"


using namespace boost::filesystem;
using namespace std;
using namespace cv;

#define DEFAULT_WINDOW_WIDTH 96
#define DEFAULT_WINDOW_HEIGHT 16

#define DEFAULT_CELL_WIDTH 4
#define DEFAULT_CELL_HEIGHT 4

#define DEFAULT_BLOCK_WIDTH 2
#define DEFAULT_BLOCK_HEIGHT 2

#define DEFAULT_SVM_GAMMA 3
#define DEFAULT_SVM_TYPE ml::SVM::C_SVC
#define DEFAULT_SVM_KERNEL ml::SVM::LINEAR


#include "opencvtools.hpp"


/**
 * @brief Uses the current trained SVM for sliding window single-scale search on the current image and shows result (may be useful for debugging)
 * 
 * @return void
 */
void HogSvmTrainer::testSvmOnCurrentImg()
{
	uint plateWidth = m_currentPlateRect.width + 2* m_marginAroundPlate_x;
	uint plateHeight = m_currentPlateRect.height + 2* m_marginAroundPlate_y;
	
	Mat scaledCurImg;
	double scaleFactorX = (double) (m_windowSize.width) / (double) plateWidth;
	double scaleFactorY = (double) (m_windowSize.height) / (double) plateHeight;
    Size scaledSize(
                cvRound( m_currentCarImg.cols * scaleFactorX ),
                cvRound( m_currentCarImg.rows * scaleFactorY )
    );
	resize(m_currentCarImg, scaledCurImg, scaledSize);
	
	vector<Rect> detectedRects;
	
	for( uint y = 0; y < (uint) scaledCurImg.rows - m_windowSize.height; y+= m_windowStrideBoosting.height ) {
		for ( uint x = 0; x < (uint) scaledCurImg.cols - m_windowSize.width; x+= m_windowStrideBoosting.width ) {
			Rect curRect = Rect(x,y,m_windowSize.width, m_windowSize.height);
			Mat window = scaledCurImg(curRect);
			vector<float> vec = extractFeatureVector(window);
			int result = (m_svm->predict(vec));
			if( result != -1) {
// 				cout << "x: " << x << ", y: " << y<< "  " << result  << endl;
				detectedRects.push_back(curRect);
// 				imwrite(to_string(x)+ " " + to_string(y) + ".png", window);
			}
		}
	}
	
		//TODO: error handling

// 	uint scaledWidth = m_c
//	Rect plateRect = Rect(Point((scaledX+0.5), (scaledY+0.5)), m_windowSize);
	

	markResultsOfHogDetector(scaledCurImg, detectedRects);
// 	rectangle(scaledCurImg, plateRect,Scalar(255,255,255),5);
	imshow("foo", scaledCurImg);
	char c = (char)cvWaitKey(250);
        if (c == 27) exit(0);
}


/**
 * @brief Uses the current trained SVM for sliding window single-scale search on a car image and shows result (may be useful for debugging)
 * 
 * @param img Car image to test the current SVM 
 * @return void
 */
void HogSvmTrainer::testSvmOnImg(Mat& img)
{
	vector<Rect> detectedRects;
	
	for( uint y = 0; y < (uint)img.rows - m_windowSize.height; y+= m_windowStrideBoosting.height ) {
		for ( uint x = 0; x < (uint)img.cols - m_windowSize.width; x+= m_windowStrideBoosting.width ) {
			Rect curRect = Rect(x,y,m_windowSize.width, m_windowSize.height);
			Mat window = img(curRect);
			vector<float> vec = extractFeatureVector(window);
			int result = (m_svm->predict(vec));
			if( result != -1) {
// 				cout << "x: " << x << ", y: " << y<< "  " << result  << endl;
				detectedRects.push_back(curRect);
// 				imwrite(to_string(x)+ " " + to_string(y) + ".png", window);
			}
		}
	}
	
	cv::Mat copyOfImg = img.clone();

	markResultsOfHogDetector(copyOfImg, detectedRects);
// 	rectangle(scaledCurImg, plateRect,Scalar(255,255,255),5);
		namedWindow("test", CV_WINDOW_AUTOSIZE);
	imshow("test", copyOfImg);
	char c = (char)cvWaitKey(0);
        if (c == 27) 
			exit(0);
}




/**
 * @brief Calculates a rectangle near the current plate position (above or below m_jaccardLimit depending on sampleClass)
 * 
 * @param sampleClass Positive samples (generated rectangle will be above m_jaccardLimit with the plate rectangle) or negative sample (generated rectangle will be below m_jaccardLimit with the plate rectangle)
 * @return cv::Rect
 */
cv::Rect HogSvmTrainer::getRectNearPlate(SampleClass sampleClass)
{
	Rect rect;
	
	int carImgHeight = m_currentCarImg.rows;
	int carImgWidth = m_currentCarImg.cols;
	int plateHeight = m_currentPlateImg.rows + 2 * m_marginAroundPlate_y;
	int plateWidth = m_currentPlateImg.cols + 2 * m_marginAroundPlate_y;
	
	
	int minX = max(0, m_currentPlateRect.x - m_marginAroundPlate_x - plateWidth );
	int maxX = min(carImgWidth - plateWidth - 1, m_currentPlateRect.x - m_marginAroundPlate_x + plateWidth);
	int minY = max(0, m_currentPlateRect.y - m_marginAroundPlate_y - plateHeight);
	int maxY = min(carImgHeight - plateHeight - 1, m_currentPlateRect.y - m_marginAroundPlate_y + plateHeight);

	uniform_int_distribution<int> distX_nearPlate(minX, maxX);
	uniform_int_distribution<int> distY_nearPlate(minY, maxY);
	
	Rect plateWithMargin( m_currentPlateRect.x - m_marginAroundPlate_x, m_currentPlateRect.y - m_marginAroundPlate_y, plateWidth, plateHeight);
	
	
	bool bAboveJaccardLimit;
	do {
		int pos_x = distX_nearPlate(m_generator);
		int pos_y = distY_nearPlate(m_generator);
		
		rect = Rect(cvPoint(pos_x,pos_y), cvPoint(pos_x + plateWidth, pos_y + plateHeight));
		bAboveJaccardLimit = calculateJaccard(rect, plateWithMargin) > m_jaccardLimit;
    } while ( sampleClass == POSITVE_SAMPLE ?  bAboveJaccardLimit : !bAboveJaccardLimit );
	
	return rect;
}


/*
HogSvmTrainer::HogSvmTrainer ( char* carImageDir, char* plateImgDir, char* negSamplesOutputDir, float jaccardLimit, double hitThreshold):
    m_carImgDir(carImageDir),
    m_plateImgDir(plateImgDir),
    m_negSamplesOutputDir(negSamplesOutputDir),
    m_carDirIterator(carImageDir),
    m_currentCarFile(),
    m_currentCarImg(),
    m_currentPlateImg(),
    m_plateSizeMatrix(),
    m_currentPlateRect(),
    m_featureVectors(),
    m_labels(),
    m_jaccardLimit(jaccardLimit),
    m_hitThreshold(hitThreshold),
    m_bPlateLocalized(false),
	m_bSvmTrained(false),
    m_numTrainedNegatives(0),
    m_numTrainedPositives(0),
    m_svm(),
    m_hog()
{
	m_windowSize = Size(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
	m_cellSize = Size(DEFAULT_CELL_WIDTH, DEFAULT_CELL_HEIGHT);
	m_blockStride = m_cellSize;
	m_blockSize= Size(DEFAULT_BLOCK_WIDTH, DEFAULT_BLOCK_HEIGHT);
	m_generator = default_random_engine(random_device{}());

	
	m_windowStrideBoosting = m_cellSize;
	checkDirectories();
	initHog();
}*/

HogSvmTrainer::HogSvmTrainer(char* carImageDir, char* plateImgDir,  float jaccardLimit, const Size& windowSize, const Size& blockStride, const Size& cellSize, const Size& blockSize, double hitThreshold):
    m_carImgDir(carImageDir),
    m_plateImgDir(plateImgDir),
    m_carDirIterator(carImageDir),
    m_currentCarFile(),
    m_currentCarImg(),
    m_currentPlateImg(),
    m_plateSizeMatrix(),
    m_currentPlateRect(),
    m_featureVectors(),
    m_labels(),
    m_jaccardLimit(jaccardLimit),
    m_hitThreshold(hitThreshold),
    m_windowSize(windowSize),
    m_cellSize(cellSize),
    m_blockSize(blockSize),
	m_blockStride(blockStride),
    m_bPlateLocalized(false),
    m_bSvmTrained(false),
    m_numTrainedNegatives(0),
    m_numTrainedPositives(0),
    m_svm(),
    m_hog(),
    m_numNegBoosting(0),
	m_numNegNearPlate(0),
	m_numNegDistantToPlate(0),
	m_numPosExactPlate(0),
	m_numPosNearPlate(0),
	m_numPosBoosting(0)
{
	m_generator = default_random_engine(12);//default_random_engine(random_device{}());
	//TODO: change this
	m_windowStrideBoosting = m_blockStride;
	checkDirectories();
	initHog();
}



HogSvmTrainer::~HogSvmTrainer() {
}

/**
* @brief Returns to first file
* 
* @return void
*/
void HogSvmTrainer::toFirstFile() {
    m_carDirIterator = directory_iterator(m_carImgDir);
}


/**
 * @brief Initializes internal cv::HOGDescriptor with the current HOG parameters
 * 
 * @return void
 */
void HogSvmTrainer::initHog()
{
	_log("Init HOG of HogSvmTrainer...");

	m_hog = new HOGDescriptor();
	m_hog->winSize = m_windowSize;
	m_hog->cellSize = m_cellSize;
	m_hog->blockSize = m_blockSize;
	m_hog->blockStride = m_blockStride;
	
}

/**
 * @brief Prints current HOG parameters to console
 * 
 * @return void
 */
void HogSvmTrainer::printHogParams() const
{
	_log("Window size: %ix%i", m_windowSize.width, m_windowSize.height);
	_log("Block size: %ix%i", m_blockSize.width, m_blockSize.height);
	_log("Block stride: %ix%i", m_blockStride.width, m_blockStride.height);
	_log("Cell size: %ix%i", m_cellSize.width, m_cellSize.height);
	
	_log("Number of histogram bins: %i", m_hog->nbins);
	_log("Window sigma: %f", m_hog->winSigma);
	_log("L2HysThreshold: %f", m_hog->L2HysThreshold);
	_log("Gamma correction: %s", m_hog->gammaCorrection ? "true" : "false");
}


/**
* @brief Indicates if there are more files in m_carImgDir to process
* 
* @return bool
*/
bool HogSvmTrainer::hasMoreFiles() const {
	
	directory_iterator it = m_carDirIterator;
	directory_iterator end_of_directory;
	while(it != end_of_directory)
	{
		if(isExistingImageFile(*it)){
			return true;
		}
		it++;
	};
	
	
    return false;
}

/**
* @brief Changes current file pointer to next file
* 
* @return void
*/
void HogSvmTrainer::nextFile() {
    assert(this->hasMoreFiles() && "there are no more images files in the current directory");

    m_bPlateLocalized = false;
	
	// go to next valid image file
	while(!isExistingImageFile(*m_carDirIterator))
	{
		m_carDirIterator++;
	};
	m_currentCarFile = *m_carDirIterator;
	m_carDirIterator++;
	
    _log("Try to open car image %s...", m_currentCarFile.c_str());
    m_currentCarImg = imread(m_currentCarFile.string(), CV_LOAD_IMAGE_GRAYSCALE);
// 	tmp.convertTo(m_currentCarImg, CV_32F);
    if(m_currentCarImg.empty()){
        _log("Failed to load %s", m_currentCarFile.c_str());    
    } else {
        path filepathPlateImg = m_plateImgDir / m_currentCarFile.filename();
        
        _log("Try to open license plate image %s...", filepathPlateImg.c_str());
        m_currentPlateImg = imread(filepathPlateImg.string(), CV_LOAD_IMAGE_GRAYSCALE);
        if(m_currentPlateImg.empty()) {
            _log("Failed to load to load plate image %s", filepathPlateImg.c_str());
        } else {
            this->localizePlate();
        }
    }
}

/**
 * @brief Localizes plate from current plate image in car image (high computational effort).
 * If plate was already found in a former run of the program, the position can be restored by previously generated plate position file.
 * 
 * @return void
 */
void HogSvmTrainer::localizePlate() {
	
	Point pos; // plate location
	assert (!m_bPlateLocalized);
	
	// Check for a location file which was possibly generated in a former run of the program
	path locationFile = m_currentCarFile;
	locationFile.replace_extension(".platelocation");
	
	if(!is_regular_file(locationFile)){
		_log("%s not found. Looking for license plate in car image...", locationFile.c_str());
		pos = findBestMatch(m_currentCarImg, m_currentPlateImg);

// 		namedWindow("my method", CV_WINDOW_AUTOSIZE);
// 		imshow("my method", m_currentPlateImg);
// 		waitKey(0);

		_log("Try to write plate location to file %s", locationFile.c_str());
		ofstream ofs;
		ofs.open(locationFile.string());
		ofs << pos.x << endl;
		ofs << pos.y << endl;
		ofs << m_currentPlateImg.cols << endl;
		ofs << m_currentPlateImg.rows << endl;
	} else {
		ifstream ifs;
		
		_log("Try to read plate location from file %s", locationFile.c_str());
		ifs.open(locationFile.string());
		
		ifs >> pos.x;
		ifs >> pos.y;
		
		
	}
	m_currentPlateRect = Rect(pos, Point( pos.x + m_currentPlateImg.cols, pos.y + m_currentPlateImg.rows));
	m_bPlateLocalized = true;
}

/**
* @brief Adds true license plate of current image to the list of positive samples (plus optional border, set m_marginAroundPlate_x/_y)
* 
* @return void
*/
void HogSvmTrainer::learnPositive()
{
	if (!m_bPlateLocalized) {
		this->localizePlate();
	}
	
	Point tl( max(m_currentPlateRect.x - m_marginAroundPlate_x , 0), max(m_currentPlateRect.y - m_marginAroundPlate_x, 0));
	Point br( min(m_currentPlateRect.br().x  + m_marginAroundPlate_y, m_currentCarImg.cols - 1),  min(m_currentPlateRect.br().y  + m_marginAroundPlate_y, m_currentCarImg.rows - 1));
	
	
	Rect plateRectWithPadding(tl,br);
	
	
	Mat pos(m_currentCarImg(plateRectWithPadding));
	this->learnPositive(pos);
	
	string curDirString = m_carImgDir.string();
	if(std::find(m_foldersUsedForTraining.begin(), m_foldersUsedForTraining.end(), curDirString) == m_foldersUsedForTraining.end()) {
		m_foldersUsedForTraining.push_back(curDirString);
	}
	
	m_numPosExactPlate++;
// 	namedWindow("opencv", CV_WINDOW_AUTOSIZE);
// 	imshow("opencv", m_currentCarImg(plateRectWithPadding));
// 	waitKey(0);
}

/**
* @brief Adds n regions of the current image to the list of negative samples
* 
* @param numNegatives Number of negative samples to generate
* @param bNearPlate Indicates whether or not the regions should be chosen to be very close (but never with a Jaccard limit greater than m_jaccardLimit) to the true plate location to improve precision of plate localization
* @return void
*/
void HogSvmTrainer::learnNegatives(unsigned int numNegatives, bool bNearPlate)
{
	
	
	if(!m_bPlateLocalized)
		this->localizePlate();
	
	
	
	int carImgHeight = m_currentCarImg.rows;
	int carImgWidth = m_currentCarImg.cols;
	int plateHeight = m_currentPlateImg.rows + m_marginAroundPlate_y * 2;
	int plateWidth = m_currentPlateImg.cols + m_marginAroundPlate_x * 2;
	
	
	
	if(!bNearPlate) {
		_log("Generate negatives samples of the hole image. Jaccard coefficient (0,%f]...", m_jaccardLimit);
		uniform_int_distribution<int> distX_holeImg(0, carImgWidth - plateWidth - 1);
        uniform_int_distribution<int> distY_holeImg(0, carImgHeight - plateHeight - 1);
		for( unsigned int i = 0; i < numNegatives; i++ ) {
			Rect sampleRect( distX_holeImg(m_generator), distY_holeImg(m_generator), plateWidth, plateHeight);
			Mat neg = m_currentCarImg(sampleRect);
			this->learnNegative(neg);
		}
		m_numNegDistantToPlate += numNegatives;
	} else {
		_log("Generate negatives samples near to the number plate. Jaccard coefficient (0,%f]...", m_jaccardLimit);

		for( unsigned int i = 0; i < numNegatives; i++ ) {
			Mat neg = m_currentCarImg(getRectNearPlate(NEGATIVE_SAMPLE));
			this->learnNegative(neg);
		}
		m_numNegNearPlate += numNegatives;
	}
	string curDirString = m_carImgDir.string();
	if(std::find(m_foldersUsedForTraining.begin(), m_foldersUsedForTraining.end(), curDirString) == m_foldersUsedForTraining.end()) {
		m_foldersUsedForTraining.push_back(curDirString);
	}
	
	m_bSvmTrained = false;
}

/**
* @brief Adds n regions of the current image very close to the true location of the plate to the list of positive samples
* 
* @param numPositives Number of positive sample to generate
* @return void
*/
void HogSvmTrainer::learnPositiveNearPlate(unsigned int numPositives)
{

	
	if(!m_bPlateLocalized)
		this->localizePlate();
	

	for( unsigned int i = 0; i < numPositives; i++ ) {
		Mat pos = m_currentCarImg(getRectNearPlate(POSITVE_SAMPLE));
		this->learnPositive(pos);
	}
	
	string curDirString = m_carImgDir.string();
	if(std::find(m_foldersUsedForTraining.begin(), m_foldersUsedForTraining.end(), curDirString) == m_foldersUsedForTraining.end()) {
		m_foldersUsedForTraining.push_back(curDirString);
	}
	
	m_numPosNearPlate += numPositives;
	
	m_bSvmTrained = false;
}

/**
 * @brief Calculates feature vector for negative sample and adds it to m_featureVectors
 * 
 * @param img Image of a negative sample
 * @return void
 */
void HogSvmTrainer::learnNegative(const Mat& img)
{
	this->learnSample(img, NEGATIVE_SAMPLE);
}


/**
 * @brief Calculates feature vector for positive sample and adds it to m_featureVectors
 * 
 * @param img Image of a positive sample
 * @return void
 */
void HogSvmTrainer::learnPositive(const cv::Mat& img)
{
	this->learnSample(img, POSITVE_SAMPLE);
}


/**
 * @brief All of the learn*-functions call this method. It extracts the HOG features and adds the feature to the current positive or negative samples
 * 
 * @param subimg Subimage used to extract HOG features
 * @param sampleClass Should the result be considered as a positive or a negative sample?
 * @return void
 */
void HogSvmTrainer::learnSample(const Mat& subimg, SampleClass sampleClass)
{	
	Mat_<float> features(extractFeatureVector(subimg));
	cv::Mat transposedFeatures;
	transpose(features, transposedFeatures);
	
	m_featureVectors.push_back(transposedFeatures);
	
	if ( sampleClass == POSITVE_SAMPLE ) {
		m_numTrainedPositives++;
		m_labels.push_back( +1 );
	} else {
		m_numTrainedNegatives++;
		m_labels.push_back( -1 );
	}
	
	bool bWriteFile = ((sampleClass == POSITVE_SAMPLE) && is_directory(m_posSamplesOutputDir )) ||
					((sampleClass == NEGATIVE_SAMPLE) && is_directory(m_negSamplesOutputDir));
					
	if( bWriteFile ) {
	
		
		path filepath = sampleClass == POSITVE_SAMPLE ?
			path(m_posSamplesOutputDir) / ("pos"+ to_string(m_numTrainedPositives) + ".png") :
			path(m_negSamplesOutputDir) / ("neg"+ to_string(m_numTrainedNegatives) + ".png");
		
			
		imwrite(filepath.c_str(), subimg);
	}
	
	
	
	m_bSvmTrained = false;
}




/**
 * @brief Updates the internal SVMs using the current positive and negative samples
 * 
 * @return void
 */
void HogSvmTrainer::trainSvm()
{
	m_svm = ml::SVM::create();
	m_svm->setType(DEFAULT_SVM_TYPE);
	m_svm->setKernel(DEFAULT_SVM_KERNEL);
	m_svm->setC(1);
	m_svm->setTermCriteria(cvTermCriteria(CV_TERMCRIT_ITER||CV_TERMCRIT_EPS , 1000, 1e-6));
	Mat weights( 1, 2, CV_32FC1 );
    weights.at<float>(0,0) = (float) 1/m_numTrainedNegatives;
    weights.at<float>(0,1) = (float) 1/m_numTrainedPositives;
	m_svm->setClassWeights( weights);
// 	svm->setGamma(DEFAULT_SVM_GAMMA); 
	
	_log("Start training SVM with %i training vectors of dimension %i...", m_labels.rows, m_featureVectors.cols);
	m_svm->train( m_featureVectors, ml::ROW_SAMPLE, m_labels );
	_log("Training finished!");
	
	
	m_bSvmTrained = true;
	
	vector<float> primalSvm;
	get_svm_detector(m_svm, primalSvm);
	m_hog->setSVMDetector(primalSvm);
}



/**
 * @brief Generate negative samples for current image via Boosting.
 * False positive hits according to the current internal SVM are added to the list of negative samples to lower the false positive rate.
 * 
 * @param jaccardBoosting Jaccard index above which detected rectangle are considered a correct detection of the current plate position
 * @return void
 */
void HogSvmTrainer::boostWithCurImage(float jaccardBoosting)
{
	_log("Boosting!!! With file %s", m_currentCarFile.c_str());
	
	// if SVM isn't up-to-date, refresh!
	if ( m_svm == nullptr ) {
		this->trainSvm();
	}

	vector< Point > foundLocations;

	vector< double > foundWeights;
	
	uint plateWidth = m_currentPlateRect.width + 2* m_marginAroundPlate_x;
	uint plateHeight = m_currentPlateRect.height + 2* m_marginAroundPlate_y;
	
	Mat scaledCurImg;

	double scaleFactorX = (double) (m_windowSize.width) / (double) plateWidth;
	double scaleFactorY = (double) (m_windowSize.height) / (double) plateHeight;
	Size scaledSize( m_currentCarImg.cols * scaleFactorX + 0.5,
					 m_currentCarImg.rows * scaleFactorY + 0.5);
	resize(m_currentCarImg, scaledCurImg, scaledSize);
// 	
#ifndef NDEBUG
	Mat cpyScaledCurImg = scaledCurImg.clone();
#endif
	m_hog->detect(scaledCurImg, foundLocations, foundWeights, m_hitThreshold, m_windowStrideBoosting , Size());
		vector<Rect> foundRects;
// // 	m_hog->detectMultiScale(scaledCurImg, foundRects, foundWeights, m_hitThreshold, m_blockStride , Size(), 1.05, 0.0, false);
	
// 	vector<int> indices = getIndexArray(foundLocations.size());
// 	sort(indices.begin(), indices.end(), indexComparator<double>(&foundWeights[0]));
	

		//TODO: error handling
	uint scaledX = (this->getCurrentPlateRect().x - m_marginAroundPlate_x) * scaleFactorX;
	uint scaledY = (this->getCurrentPlateRect().y - m_marginAroundPlate_y) * scaleFactorY;
// 	uint scaledWidth = m_c
	Rect plateRect = Rect(Point((scaledX+0.5), (scaledY+0.5)), m_windowSize);
	
	unsigned int numTruePos = 0;
	unsigned int numFalsePos = 0;
	
		
	
	for ( uint i = 0; i < foundLocations.size(); i++ ) {
// 		Rect detectedRect = foundRects[i];
		Rect detectedRect = Rect(foundLocations[i], m_windowSize);
		foundRects.push_back(detectedRect);
		if (calculateJaccard(plateRect, detectedRect) < jaccardBoosting ) {
			// wrongly detected! boost!
			numFalsePos++;
			this->learnNegative(scaledCurImg(detectedRect));
			m_numNegBoosting++;
		} else {
// 			this->learnPositive(scaledCurImg(detectedRect));
// 			m_numPosBoosting++;
			numTruePos++;
		}
// 		foundRects.push_back(detectedRect);
	}
	
#ifndef NDEBUG
	rectangle(cpyScaledCurImg, plateRect, Scalar(255,255,255));
	markResultsOfHogDetector(cpyScaledCurImg, foundRects, foundWeights, true);
	
	imshow("opencv", cpyScaledCurImg);
	
       char c = (char)cvWaitKey(250);
        if (c == 27) exit(0);
#endif
// 			

// 	waitKey(0);
	_log("%s: %i true positive, %i false positive", m_currentCarFile.c_str(), numTruePos, numFalsePos);

}




Ptr< ml::SVM > HogSvmTrainer::getSvm()
{
	if( !m_bSvmTrained ) {
		this->trainSvm();
	}
	return m_svm;
}


/**
 * @brief Extracts a HOG feature of an (sub-) image using the currently set HOG parameters
 * 
 * Calls cv::HOGDescriptor::compute after a prior size normalization of the image.
 * 
 * @param plateSizeImg ...
 * @return std::vector< float, std::allocator< void > >
 */
vector< float > HogSvmTrainer::extractFeatureVector(const Mat& plateSizeImg)
{
	// normalize size of image
	resize(plateSizeImg, m_plateSizeMatrix, m_windowSize);
	
	// calculate feature vector
	vector< float > rtn;
	m_hog->compute(m_plateSizeMatrix, rtn);
	
	return rtn;
}

/**
 * @brief Checks if currently set directories exists and returns error in the negative case
 * 
 * @return void
 */
void HogSvmTrainer::checkDirectories()
{

	if(!is_directory(m_carImgDir)){
		_error("%s does not exist", m_carImgDir.c_str());
	}
	if(!is_directory(m_plateImgDir)){
		_error("%s does not exist", m_carImgDir.c_str());
	}
	if(!is_directory(m_negSamplesOutputDir)){
		_log("Negative sample output directory does not exist: %s", m_negSamplesOutputDir.c_str());
	}
	if(!is_directory(m_posSamplesOutputDir)){
		_log("Negative sample output directory does not exist: %s", m_negSamplesOutputDir.c_str());
	}
}

/**
 * @brief Saves SVM generated by current positive and samples to file. trainSvm() is implicitly called
 * 
 * @param filename Absolute or relative path to destination file 
 * @return void
 */
void HogSvmTrainer::saveSvm(const string& filename)
{
	if ( !m_bSvmTrained ) {
		this->trainSvm();
	}
	
	m_svm->save(filename);

	ofstream ofs;
	ofs.open(filename, std::ios_base::app);
	ofs << endl;
	ofs << "#Parameters used for training:" << endl;
	ofs << "#\twindow size: " << m_windowSize.width << "x" << m_windowSize.height << endl;
	ofs << "#\tblock size: " << m_blockSize.width << "x" << m_blockSize.height << endl;
	ofs << "#\tblock stride: " << m_blockStride.width << "x" << m_blockStride.height << endl;
	ofs << "#\tcell size: " << m_cellSize.width << "x" << m_cellSize.height << endl;
	ofs << "#\tmargin around plate: " << m_marginAroundPlate_x << "x" << m_marginAroundPlate_y << " (at least for the last samples if it was changed during training)" << endl;
	ofs << endl;
	
	ofs << "#Number of positive samples: " << m_numTrainedPositives << endl;
	ofs << "#\t- " << m_numPosExactPlate << " exact plates" << endl;
	ofs << "#\t- " << m_numPosNearPlate << " rectangles near plate" << endl;
	ofs << "#\t- " << m_numPosBoosting << " rectangles generated by boosting" << endl;
	ofs << "#\t- " << m_numTrainedPositives - ( m_numPosExactPlate + m_numPosBoosting + m_numPosNearPlate) << " other origin (e.g. loaded from directory)" << endl;
	ofs << endl;
	
	ofs << "#Number of negative samples: " << m_numTrainedNegatives << endl;
	ofs << "#\t- " << m_numNegNearPlate << " rectangles near plate" << endl;
	ofs << "#\t- " << m_numNegDistantToPlate << " random rectangles form rest of the image" << endl;
	ofs << "#\t- " << m_numNegBoosting << " rectangles generated by boosting" << endl;
	ofs << "#\t- " << m_numTrainedNegatives - ( m_numNegBoosting + m_numNegDistantToPlate + m_numPosNearPlate ) << "  other origin (e.g. loaded from directory)" << endl;
	ofs << endl;
	ofs << "#Folders used for training:" << endl;
	for( string& dir : m_foldersUsedForTraining ) {
		ofs << "#\t- " << dir << endl;
	}
	
	_log("Saved SVM to %s", filename.c_str());
}


/**
 * @brief Reads previously generated SVM from a file (should be only used for debugging). 
 * 
 * Better use readModelFile() which saves all parameters of a previous state of HogSvmTrainer
 * 
 * @param filename Absolute or relative path to source file
 * @return void
 */
void HogSvmTrainer::readSvm(const string& filename)
{
	//TODO: remove
	_log("Warning: Only use this for debugging!");
	if ( !is_regular_file(path(filename))) {
		_error("SVM file does not exist");
		exit( EXIT_FAILURE );
	}
	
	m_svm = ml::StatModel::load<ml::SVM>(filename);
}



/**
 * @brief Saves current feature vectors (depended on HOG parameters) generated by positive and negative samples to file to possibly restore the current state of HogSvmTrainer later,
 * 
 * @param filename Relative or absolute path to destination file
 * @return void
 */
void HogSvmTrainer::saveModelFile(const string& filename) const
{
	_log("Try to save model file to %s", filename.c_str());
	
	ofstream ofs(filename.c_str());
	boost::archive::binary_oarchive oa(ofs);
	
	oa << m_numTrainedPositives << m_numTrainedNegatives;
	oa << m_featureVectors;
	oa << m_labels;
	oa << m_windowSize.width << m_windowSize.height;
	oa << m_blockSize.width << m_blockSize.height;
	oa << m_blockStride.width << m_blockStride.height;
	oa << m_cellSize.width << m_cellSize.height;
	
	oa << m_hog->nbins;
	oa << m_hog->winSigma;
	oa << m_hog->L2HysThreshold;
	oa << m_hog->gammaCorrection;
	
	oa << m_numNegBoosting;
	oa << m_numNegNearPlate;
	oa << m_numNegDistantToPlate;
	oa << m_numPosExactPlate;
	oa << m_numPosNearPlate;
	oa << m_numPosBoosting;
	
	_log("Saved model file to %s", filename.c_str());
	_log("Saved %i positive samples, %i negatives (total %i)", m_numTrainedPositives, m_numTrainedNegatives, m_numTrainedPositives+m_numTrainedNegatives);

	this->printHogParams();
}

/**
* @brief Restores previously saved feature vector of positive and negative samples
* 
* @param filename Relative or absolute path of source file
* @return void
*/
void HogSvmTrainer::readModelFile(const string& filename)
{
	_log("Try to read model file from %s", filename.c_str());
	if (!is_regular_file(path(filename))){
		_error("Could not open model file \"%s\"", filename.c_str());
		throw std::runtime_error("Could not open model file \"" + filename +"\"");
	}
	
	std::ifstream ifs(filename.c_str());
	boost::archive::binary_iarchive ia(ifs);

	ia >> m_numTrainedPositives;
	ia >> m_numTrainedNegatives;
	ia >> m_featureVectors;
	ia >> m_labels;
	ia >> m_windowSize.width >> m_windowSize.height;
	ia >> m_blockSize.width >> m_blockSize.height;
	ia >> m_blockStride.width >> m_blockStride.height;
	ia >> m_cellSize.width >> m_cellSize.height;	
	
	
	m_bSvmTrained = false;
	initHog();
	
	ia >> m_hog->nbins;
	ia >> m_hog->winSigma;
	ia >> m_hog->L2HysThreshold;
	ia >> m_hog->gammaCorrection;
	
	ia >> m_numNegBoosting;
	ia >> m_numNegNearPlate;
	ia >> m_numNegDistantToPlate;
	ia >> m_numPosExactPlate;
	ia >> m_numPosNearPlate;
	ia >> m_numPosBoosting;
	
	_log("Read model file from %s", filename.c_str());
	_log("Loaded %i positive samples, %i negatives (total %i)", m_numTrainedPositives, m_numTrainedNegatives, m_numTrainedPositives+m_numTrainedNegatives);

	this->printHogParams();
}

/**
 * @brief Calculates feature vectors from negative samples saved in a directory.
 * 
 * @param dir Directory with images of negative samples
 * @return void
 */
void HogSvmTrainer::learnNegativesFromDirectory(const string& dir)
{
	this->learnSamplesFromDirectory(dir, NEGATIVE_SAMPLE);
}

/**
 * @brief Calculates feature vectors from positive samples saved in a directory.
 * 
 * @param dir Directory with images of positive samples
 * @return void
 */
void HogSvmTrainer::learnPositivesFromDirectory(const string& dir)
{
	this->learnSamplesFromDirectory(dir, POSITVE_SAMPLE);
}

/**
 * @brief Calculates feature vectors of samples saved in a directory of one class (positive or negative).
 * 
 * @param dir Directory with images of positive samples
 * @param sampleClass Class of the samples (positive or negative)
 * @return void
 */
void HogSvmTrainer::learnSamplesFromDirectory(const string& dir, SampleClass sampleClass)
{
	path directory(dir);
	Mat sample;
	
	if(is_directory(directory)) {
		directory_iterator end_of_directory;
		for( directory_iterator it(directory); it != end_of_directory; it++ ){
			if(isExistingImageFile(*it)) {
				sample = imread(path(*it).string(), CV_LOAD_IMAGE_GRAYSCALE);
				if( sample.empty() ) {
					_error("Failed to read file \"%s\".", path(*it).c_str());
				} else {
					this->learnSample(sample, sampleClass);
					_log("Learned file \"%s\" as a %s sample.", path(*it).c_str(), sampleClass == POSITVE_SAMPLE ? "positive":"negative");
				}
			}
		}
		if(std::find(m_foldersUsedForTraining.begin(), m_foldersUsedForTraining.end(), dir) == m_foldersUsedForTraining.end()) {
			m_foldersUsedForTraining.push_back(dir);
		}
	} else {
		_error("Failed to learn from \"%s\"! Directory does not exist.", dir.c_str());
	}
}




