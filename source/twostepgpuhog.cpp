#include "twostepgpuhog.hpp"
#include "opencvtools.hpp"

#include <opencv2/cudawarping.hpp>
#include <opencv2/core/cuda_stream_accessor.hpp>
#include <algorithm>

using namespace cv;
using namespace std;


TwoStepGpuHog::TwoStepGpuHog():
    TwoStepHog()
{
    m_numBinsCoarse = 9;
    m_numBinsFine = 9;
    m_bHogInitialized = false;

    m_hitsAsyncCoarseFrontBuffer = new vector<vector<Point>>();
    m_weightsAsyncCoarseFrontBuffer = new vector<vector<double>>();
    m_hitsAsyncCoarseBackBuffer = new vector<vector<Point>>();
    m_weightsAsyncCoarseBackBuffer = new vector<vector<double>>();

    m_coarseBuffersFrontBuffer = new std::vector<cv::cuda::GpuMat>();
    m_coarseBuffersBackBuffer = new std::vector<cv::cuda::GpuMat>();
}

TwoStepGpuHog::~TwoStepGpuHog()
{
    delete m_hitsAsyncCoarseFrontBuffer;
    delete m_weightsAsyncCoarseFrontBuffer;
    delete m_hitsAsyncCoarseBackBuffer;
    delete m_weightsAsyncCoarseBackBuffer;
    delete m_coarseBuffersFrontBuffer;
    delete m_coarseBuffersBackBuffer;
}

void TwoStepGpuHog::setSvmCoarseDectector(cv::Ptr< cv::ml::SVM >& svm)
{
    m_svmCoarse = svm;
}

void TwoStepGpuHog::setSvmFineDectector(cv::Ptr< cv::ml::SVM >& svm)
{
    m_svmFine = svm;
}

void TwoStepGpuHog::initHogs()
{


    m_hogCoarseSearch = GPU_HOG::create(m_winSizeCoarse, m_blockSizeCoarse, m_blockStrideCoarse, m_cellSizeCoarse, m_numBinsCoarse);
    m_hogFineSearch = GPU_HOG::create(m_winSizeFine, m_blockSizeFine, m_blockStrideFine, m_cellSizeFine, m_numBinsFine);

    m_hogCoarseSearch->setWinStride(m_winStrideCoarse);
    m_hogFineSearch->setWinStride(m_winStrideFine);

    m_hogCoarseSearch->setSVMDetector(get_svm_detector(m_svmCoarse));
    m_hogFineSearch->setSVMDetector(get_svm_detector(m_svmFine));

    m_hogCoarseSearch->setHitThreshold(m_hitThresholdCoarse);
    m_hogFineSearch->setHitThreshold(m_hitThresholdFine);

    m_bHogInitialized = true;
}
/*
void TwoStepGpuHog::detect(cuda::GpuMat gpuImg, std::vector< cv::Rect >& foundRect, vector< double >& confValues)
{
    LOG_CALL;
    if(!m_bHogInitialized){
        initHogs();
    }
    cuda::GpuMat scaledImg;
    cuda::GpuMat subImg;
    cuda::GpuMat scaledSubImg;

    for ( double curScaleCoarse : m_scalesCoarse ) {

        Size localSearchSize( m_winSizeCoarse.width + 2 * m_marginAroundCoarseHit_x, m_winSizeCoarse.height + 2 * m_marginAroundCoarseHit_y );

        if( curScaleCoarse == 1.0 ) {
            scaledImg = gpuImg;
        } else {
            Size scaledSize = Size(
                cvRound( gpuImg.cols / curScaleCoarse ),
                cvRound( gpuImg.rows / curScaleCoarse )
            );
            cuda::resize(gpuImg, scaledImg, scaledSize);
        }

        vector<Point> foundLocationsCoarse;
        vector<double> foundWeightsCoarse;
        _log("Start coarse detection for scale %f", curScaleCoarse);
        m_hogCoarseSearch->detect(scaledImg, foundLocationsCoarse, &foundWeightsCoarse);
        _log("Coarse detection for scale %f", curScaleCoarse);
        for( Point& curPoint : foundLocationsCoarse ) {
            foundRect.push_back( Rect(curScaleCoarse * curPoint, curScaleCoarse * m_winSizeCoarse));
        }
        confValues.insert(confValues.end(), foundWeightsCoarse.begin(), foundWeightsCoarse.end());

       assert(confValues.size() == foundRect.size());

       startFineSearchCpuTimeMeasurement();

       for ( uint j = 0; j < foundLocationsCoarse.size(); j++ ) {
            Rect localSearchArea( Point(foundLocationsCoarse[j].x - m_marginAroundCoarseHit_x, foundLocationsCoarse[j].y - m_marginAroundCoarseHit_y), localSearchSize );
            cropRect(localSearchArea, Size(scaledImg.cols, scaledImg.rows));

            subImg = scaledImg(localSearchArea);

            for ( double curScaleFine : m_scalesFine ) {
                if ( curScaleFine == 1.0 ) {
                    scaledSubImg = subImg;
                } else {
                    Size scaledSubSize = Size(
                        cvRound( subImg.cols / curScaleFine ),
                        cvRound( subImg.rows / curScaleFine )
                    );
                    cuda::resize(subImg, scaledSubImg, scaledSubSize);
                }

                vector<Point> foundLocationsFine;
                vector<double> foundWeightsFine;
                _log("Start a fine detection for scale %f (coarse scale %f, fine scale %f)", curScaleCoarse*curScaleFine , curScaleCoarse, curScaleFine);
                m_hogFineSearch->detect(scaledSubImg, foundLocationsFine, &foundWeightsFine);
                Size scaledWinSize( m_winSizeFine.width / curScaleCoarse, m_winSizeFine.height / curScaleCoarse );
                for ( Point& curPoint : foundLocationsFine ) {
                    foundRect.push_back( Rect(curScaleCoarse*((curScaleFine*curPoint) + localSearchArea.tl()), curScaleCoarse * curScaleFine * m_winSizeFine));
                }
                confValues.insert(confValues.end(), foundWeightsFine.begin(), foundWeightsFine.end());
                assert(confValues.size() == foundRect.size());
            }

        }
        endFineSearchCpuTimeMeasurement(foundLocationsCoarse.size());
    }
}*/


	/**
	 * @brief Performs a two-step sliding window search on multi-scales to find license plates in a image (coarse + refinement search)
	 * 
	 * @param img Image on GPU on which the search for license plates should be performed
	 * @param foundRect Detected plate rectangles
	 * @param confValues Confidence values of each detected rectangle
	 * @return void
	 */
void TwoStepGpuHog::detect(cv::cuda::GpuMat& gpuImg, std::vector< cv::Rect >& foundRect, std::vector< double >& confValues)
{
    LOG_CALL;
    if(!m_bHogInitialized){
        initHogs();
    }

    vector<double> scales;

    this->performSearchAsync(gpuImg, SEARCH_TYPE_COARSE);
#ifdef CUSTOM_OCV
    m_hogCoarseSearch->synchronize();
#endif
    this->processSearchAsync(foundRect, nullptr, &scales, SEARCH_TYPE_COARSE);

}



void TwoStepGpuHog::detect(Mat& img, vector< Rect >& foundRect, vector< double >& confValues)
{
    LOG_CALL;
    if(!m_bHogInitialized){
        initHogs();
    }
    cuda::GpuMat gpuImg;
    gpuImg.upload(img);

    assert(!gpuImg.empty() && "Failed to upload image");

    this->detect(gpuImg, foundRect, confValues);
}

void TwoStepGpuHog::detectRealTime(Ptr<FrameSource> source, bool bShowResults)
{
	LOG_CALL;
    cuda::GpuMat curGpuImg;
    cuda::GpuMat prevGpuImg;
    vector<Rect> localResults;
    vector<double> localWeights;

    vector< vector< double > > foundScales;
	vector< vector< Rect > > foundRects;
	vector< vector< double > > foundWeights;

    uint numFrames = source->getNumberOfFrames();
    foundScales.resize(numFrames);
    foundRects.resize(numFrames);
    foundWeights.resize(numFrames);
		
    if(!m_bHogInitialized) {
        initHogs();
    }

    if(bShowResults) {
        namedWindow("opencv", CV_WINDOW_AUTOSIZE );
    }

    Mat curFrame;
	Mat prevFrame;

// 	vector<Rect> globalResults;

	for( uint frameIdx = 0; frameIdx < numFrames + 1; frameIdx++ ) {
        time_t begin = clock();
		
		
        if ( source->hasMoreFrames() ) {
			source->loadFrameIntoMat(curFrame);
#ifdef CUSTOM_OCV
            cuda::Stream coarseStream = m_hogCoarseSearch->getStream();
            curGpuImg.upload(curFrame, coarseStream);
#else
            curGpuImg.upload(curFrame);
#endif
            performSearchAsync(curGpuImg, SEARCH_TYPE_COARSE);
        }
        // process previous frame
        if( frameIdx != 0 ) {


//            for(uint lev = 0; lev < m_hitsAsyncCoarseBackBuffer->size(); lev++){
//                for(uint hitIdx = 0; hitIdx < m_hitsAsyncCoarseBackBuffer->at(lev).size(); hitIdx++ ){
//                    rectangle(img[i-1], Rect(m_hitsAsyncCoarseBackBuffer->at(lev)[hitIdx], m_winSizeCoarse * m_scalesCoarse[lev]), Scalar(255,255,255), 2 );

//                }
//            }
//            imshow("opencv", img[i-1]);

//             if ( 27 == waitKey(0) ) {
//                 exit ( EXIT_SUCCESS );
//             }

            for ( uint levIdx = 0; levIdx < m_hitsAsyncCoarseBackBuffer->size(); levIdx++ ) {


                vector<Rect> globalResults;
                nonMaximumSupression((*m_hitsAsyncCoarseBackBuffer)[levIdx], m_winSizeCoarse, globalResults);

//                for( uint k = 0; k < m_hitsAsyncCoarseBackBuffer->at(levIdx).size(); k++ ) {
                for( uint k = 0; k < globalResults.size(); k++ ) {
                    Rect curRect = globalResults[k];

                    assert( curRect.x >= 0);
                    cuda::GpuMat scaledImg = m_coarseBuffersBackBuffer->at(levIdx);

                    Point localRectTL =  curRect.tl() - Point(m_marginAroundCoarseHit_x, m_marginAroundCoarseHit_y);


                    Rect localSearchArea( localRectTL, m_winSizeCoarse + Size(2*m_marginAroundCoarseHit_x, 2* m_marginAroundCoarseHit_y) );

                    cropRect(localSearchArea, scaledImg.size());
                    cuda::GpuMat subRect = scaledImg(localSearchArea);
                    Mat subRectCpu;
//                    subRect.download(subRectCpu);
//                    imshow("opencv", subRectCpu);
//                    if ( 27 == waitKey(0) ) {
//                        exit ( EXIT_SUCCESS );
//                    }

                    performSearchAsync(subRect, SEARCH_TYPE_FINE);
#ifdef CUSTOM_OCV
                    m_hogFineSearch->synchronize();
#endif
                    processSearchAsync(localResults, &localWeights, nullptr, SEARCH_TYPE_FINE);


                    if ( !localResults.empty() ) {
                        int argMax = -1;
                        double max = numeric_limits<double>::min();

                        for( uint l = 0; l < localWeights.size(); l++ ) {
                            if (localWeights[l] > max ) {
                                max = localWeights[l];
                                argMax = l;

                            }
                        }
                        foundRects[ frameIdx - 1 ].push_back( (localResults[argMax] + localSearchArea.tl()) * m_scalesCoarse[levIdx] );
                        foundWeights[ frameIdx - 1 ].push_back( max );
                        foundScales[ frameIdx - 1].push_back( m_scalesCoarse[levIdx] * m_scalesFine[argMax] );
                    }
                    nonMaximumSupression(foundRects[ frameIdx - 1 ], nullptr);
                    //groupRectangles_meanshift(foundRect[ frameIdx - 1 ], confValues[ frameIdx - 1 ], foundScales[ frameIdx - 1], 1.0, m_winSizeCoarse);
                }
            }

            for(uint hitIdx = 0; hitIdx < foundRects[frameIdx-1].size(); hitIdx++ ){
                rectangle(prevFrame, foundRects[frameIdx-1][hitIdx], Scalar(255,255,255), 2 );

            }
            time_t end = clock();

            double time_per_frame = double(end - begin) / CLOCKS_PER_SEC;
            double fps = 1 / time_per_frame;
            if(bShowResults) {

                putText(prevFrame, to_string(fps) + " fps", Point(50,50), 0, 0.5, Scalar(255,255,255));
                imshow("opencv", prevFrame);

                 if ( 27 == waitKey(5) ) {
                     exit ( EXIT_SUCCESS );
                 }
            } else {
                _log("Processing at %f fps", fps);
            }


        }
        if ( frameIdx != numFrames ) {
#ifdef CUSTOM_OCV
            m_hogCoarseSearch->synchronize();
#endif
			prevGpuImg = curGpuImg;
			prevFrame = curFrame;
            swap(m_hitsAsyncCoarseFrontBuffer, m_hitsAsyncCoarseBackBuffer);
            swap(m_weightsAsyncCoarseFrontBuffer, m_weightsAsyncCoarseBackBuffer);
            swap(m_coarseBuffersFrontBuffer, m_coarseBuffersBackBuffer);
        }
    }
}



void TwoStepGpuHog::detectRealTime(vector< Mat >& img, vector< vector< Rect > >& foundRect, vector< vector< double > >& confValues, bool bShowResults )
{
	assert( false && "Not implemented" );
}



/**
 * @brief Starts a search using either m_hogCoarseSearch or m_hogFineSearch. If the custom class cv::cuda::AsynchrousHog is 
 * available, the search is performed without using blocking cuda calls on a respective own stream for fine or for coarse search.
 * Saves raw results to private members of TwoStepGpuHog (that have buffer in their name).
 * 
 * @param gpuImg Image on which the search should be performed.
 * @param fineOrCoarse Fine or coarse search: SEARCH_TYPE_COARSE or SEARCH_TYPE_FINE
 * @return void
 */
void TwoStepGpuHog::performSearchAsync(cuda::GpuMat& gpuImg, TypeOfSearch fineOrCoarse)
{
LOG_CALL;
    cuda::GpuMat scaledImg;



    bool bCoarseSearch = ( fineOrCoarse == SEARCH_TYPE_COARSE );
    const uint numScales = bCoarseSearch? m_scalesCoarse.size() : m_scalesFine.size();
    cuda::Stream resizeStream;
    if(bCoarseSearch){
        m_hitsAsyncCoarseFrontBuffer->resize(numScales);
        m_weightsAsyncCoarseFrontBuffer->resize(numScales);
        m_hitsAsyncCoarseBackBuffer->resize(numScales);
        m_weightsAsyncCoarseBackBuffer->resize(numScales);

        m_coarseBuffersFrontBuffer->resize(numScales);
        m_coarseBuffersBackBuffer->resize(numScales);
#ifdef CUSTOM_OCV
        resizeStream = m_hogCoarseSearch->getStream();
#endif
    } else {
        m_fineBuffers.resize(numScales);
        m_hitsAsyncFine.resize(numScales);
        m_weightsAsyncFine.resize(numScales);
#ifdef CUSTOM_OCV
        resizeStream = m_hogFineSearch->getStream();
#endif
    }



    for( uint i = 0; i < numScales; i++ ) {

        double curScale = bCoarseSearch ? m_scalesCoarse[i] : m_scalesFine[i];
        if( abs(curScale - 1.0) < 1e-3 ) {
            if(bCoarseSearch) {
                m_coarseBuffersFrontBuffer->at(i) = gpuImg;
            } else {
                m_fineBuffers[i] = gpuImg;
            }
        } else {
            Size scaledSize = Size(
                cvRound( gpuImg.cols / curScale ),
                cvRound( gpuImg.rows / curScale )
            );
            if(bCoarseSearch) {
                cuda::resize(gpuImg, m_coarseBuffersFrontBuffer->at(i), scaledSize, 0., 0., INTER_LINEAR, resizeStream);
            } else {
                cuda::resize(gpuImg, m_fineBuffers[i], scaledSize, 0., 0., INTER_LINEAR, resizeStream);
            }
        }


         if(bCoarseSearch){

             _log("Start coarse detection for scale %f", curScale);
            m_hogCoarseSearch->detect((*m_coarseBuffersFrontBuffer)[i], (*m_hitsAsyncCoarseFrontBuffer)[i], &((*m_weightsAsyncCoarseFrontBuffer)[i]));
         } else {

             _log("Start fine detection for scale %f", curScale);
            m_hogFineSearch->detect(m_fineBuffers[i], m_hitsAsyncFine[i], &(m_weightsAsyncFine[i]));
         }
    }
}

/**
 * @brief Processes the the raw results of a previous call 
 * 
 * @param hits Detected results
 * @param weights ...
 * @param scales ...
 * @param fineOrCoarse ...
 * @return void
 */
void TwoStepGpuHog::processSearchAsync(vector< Rect >& hits, vector< double >* weights, vector< double >* scales, TypeOfSearch fineOrCoarse)
{
    LOG_CALL;



    hits.clear();
    if(weights){
        weights->clear();
    }
    if (scales) {
        scales->clear();
    }

    bool bCoarseSearch = ( fineOrCoarse == SEARCH_TYPE_COARSE );

    if( bCoarseSearch) {
        for( uint i = 0; i < m_hitsAsyncCoarseBackBuffer->size(); i++ ) {
            for( Point& curPoint : (*m_hitsAsyncCoarseBackBuffer)[i] ) {
                hits.push_back( Rect(m_scalesCoarse[i] * curPoint, m_scalesCoarse[i] * m_winSizeCoarse));
                if(scales){
                    scales->push_back(m_scalesCoarse[i]);
                }
            }

            if(weights) {
                weights->insert(weights->end(), (*m_weightsAsyncCoarseBackBuffer)[i].begin(), (*m_weightsAsyncCoarseBackBuffer)[i].end());
            }

        }
    } else {

        for( uint i = 0; i < m_hitsAsyncFine.size(); i++ ) {
            for( Point& curPoint : m_hitsAsyncFine[i] ) {
                hits.push_back( Rect(m_scalesFine[i] * curPoint, m_scalesFine[i] * m_winSizeFine));
                if(scales){
                    scales->push_back(m_scalesFine[i]);
                }
            }

            if(weights) {
                weights->insert(weights->end(), m_weightsAsyncFine[i].begin(), m_weightsAsyncFine[i].end());
            }

        }
    }
}


