#include "printinformation.hpp"

#include <iostream>
#include <stdlib.h>
#include <boost/filesystem.hpp>

#include "log.hpp"
#include "filetools.hpp"

using namespace std;
using namespace boost::filesystem;

/**
 * @brief Prints information on program (at the moment only its name)
 * 
 * @return void
 */
void printProgramInfo()
{
    cout << "License Plate Detector" << endl;
    cout << endl;
}

/**
 * @brief Prints information on command line usage of program
 * 
 * @return void
 */
void printHelp()
{
    cout << "Usage: main [OPTIONS] IMAGE_FILE" << endl;
    cout << "       main [OPTIONS] VIDEO_FILE" << endl;
    cout << "       main [OPTIONS] INPUT_DIRECTORY" << endl;
	cout << "       main --generateNegatives TRAINING_CAR_IMAGE_DIRECTORY TRAINING_PLATE_ONLY_IMAGE_DIRECTORY OUTPUT_DIR_NEGATIVES" << endl;
//     cout << endl;
    
    cout << "Possible options:" << endl;
    cout << "\t--gpu" << endl;
    cout << "\t\t Executes GPU version of the algorithm" << endl;
    cout << "\t--cpu" << endl;
    cout << "\t\t Executes CPU version of the algorithm" << endl;
	cout << endl;
	cout << "--generateNegatives or -gn" << endl;
	cout << "\tGenerates negative samples for a given training set" << endl;
	cout << endl;
}

/**
 * @brief Prints error "Invalid Arguments or incorrect usage!" and exits
 * 
 * @return void
 */
void errorInvalidArgs()
{
	_error("Invalid Arguments or incorrect usage!");
	printHelp();
	exit(EXIT_FAILURE);
}

/**
 * @brief Prints error "###  is not a directory! Exiting..." and exits
 * 
 * @return void
 */
void errorAndExitIfNotADirectory(char* directory)
{
	if(!is_directory(directory)) {
		_error("\"%s\" is not a directory! Exiting...", directory);
		exit(EXIT_FAILURE);
	}
}

