#include "imagetools.hpp"
#include "log.hpp"

#include <algorithm>
#include <ctime>
#include <iostream>



using namespace cv;
using namespace std;

/**
 * @brief Returns gray channel of RGB image
 * 
 * @param imgRGB RGB-image
 * @return cv::Mat Gray channel of colored RGB input image
 */
cv::Mat getGrayChannel(const cv::Mat& imgRGB)
{
    if( imgRGB.empty() )
        return cv::Mat();
	Mat imgYCrCb;
	cv::cvtColor(imgRGB, imgYCrCb, CV_BGR2YCrCb);

	Mat tmp[3];
	split(imgYCrCb, tmp);
	return tmp[0];
}

static int imgDiffAtPosition(const Mat& img, const Mat& plateImg, const int x, const int y, const int maxTolerableDiff)
{
    int plateHeight = plateImg.rows;
    int plateWidth = plateImg.cols;
    
    int sumOfAbsDiff = 0;
    int maxDiff = 0;
    
    for( int y_diff = 0; y_diff < plateHeight; y_diff++ ) {
        for( int x_diff = 0; x_diff < plateWidth; x_diff++ ) {
            int diff = abs((int)img.at<uchar>(y + y_diff, x + x_diff ) - (int) plateImg.at<uchar>(y_diff, x_diff));
            sumOfAbsDiff += diff;
            maxDiff = max(maxDiff, diff);
            
            if ( sumOfAbsDiff > maxTolerableDiff ) 
                return INT_MAX;
        }
    }
            //  cout << sumOfAbsDiff << " (" << x << "," << y << ")" << endl;
                            return sumOfAbsDiff;
}

/**
 * @brief Finds subimage in image or the position in the bigger image with least deviation to the subimage
 * 
 * @param img Bigger image to look into
 * @param subImg Small image to find
 * @return cv::Point: Found location
 */
CvPoint findBestMatch( const cv::Mat& img, const cv::Mat& subImg )
{
    int plateWidth = subImg.cols;
    int plateHeight = subImg.rows;
    int imgWidth = img.cols;
    int imgHeight = img.rows;
    
    CvPoint foundAtPosition;
    
    int minDiff = INT_MAX;
    for( int y = 0; y < imgHeight - plateHeight + 1; y++ ) {
        for( int x = 0; x < imgWidth - plateWidth + 1; x++ ) {
            int result = imgDiffAtPosition(img, subImg, x, y, minDiff);
            if( result < minDiff) {
                foundAtPosition.x = x;
                foundAtPosition.y = y;
                minDiff = result; 
            }
        }
    }
    return foundAtPosition;
}


#define MAX_RECTS_MARKED 1000000

void markResultsOfHogDetector(Mat& img, const vector< Rect >& foundRects, const vector< double >& foundWeights, bool bTextOutput, const Scalar& primaryColor, const Scalar& secondaryColor)
{

    assert(foundWeights.size() == foundRects.size() || foundWeights.empty());


    double maxVal = std::numeric_limits<double>::min();
	Rect argMax;

	for ( int i = 0; i < min(MAX_RECTS_MARKED, (int)foundRects.size()); i++){
        if ( !foundWeights.empty() && foundWeights.at(i) > maxVal ) {
			maxVal = foundWeights.at(i);
			argMax = foundRects.at(i);

			
		}
// 		if(foundWeights.at(i) > 2){
			rectangle(img, foundRects.at(i).tl(), foundRects.at(i).br(), secondaryColor, 0.25);
// 		}
	}
    if(!foundWeights.empty())
        rectangle(img, argMax.tl(), argMax.br(), primaryColor, 2);
	
	if(bTextOutput) {
		_log("Detected rectangles: %i" , (int) foundRects.size());
		_log("Max confidence: %f" , maxVal);
	}
/*	
	vector<Rect> found_filtered;
	
	size_t i, j;
	for (i=0; i< min(2, (int) foundRects.size()); i++) 
	{
		Rect r = foundRects[i];
		for (j=0; j<foundRects.size(); j++) 
			if (j!=i && (r & foundRects[j]) == r)
				break;
			if (j == foundRects.size())
				found_filtered.push_back(r);
	}
	
	for ( i=0; i<found_filtered.size(); i++ ) {
		Rect r = found_filtered[i];
// 		r.x += cvRound ( r.width*0.1 );
// 		r.width = cvRound ( r.width*0.8 );
// 		r.y += cvRound ( r.height*0.07 );
// 		r.height = cvRound ( r.height*0.8 );
		cv::rectangle ( img, r.tl(), r.br(), primaryColor, 3 );
	}*/
}


void markResultsOfHogDetector(Mat& img, const vector< Rect >& foundRects, bool bTextOutput, const Scalar& primaryColor)
{


	for( int i = 0; i < min(MAX_RECTS_MARKED, (int)foundRects.size()); i++){
		rectangle(img, foundRects.at(i).tl(), foundRects.at(i).br(), primaryColor, 0.25);
	}

	
	if(bTextOutput) {
		_log("Detected rectangles: %i" , (int)foundRects.size());



	}

}
    
