#include "opencvtools.hpp"

#include <limits>
#include <assert.h>

using namespace cv;
using namespace std;

// from: opencv 3.0  samples/cpp/train_HOG.cpp
void get_svm_detector(const Ptr< cv::ml::SVM >& svm, vector< float >& hog_detector )
{
    // get the support vectors
    Mat sv = svm->getSupportVectors();
    const int sv_total = sv.rows;
    // get the decision function
    Mat alpha, svidx;
    double rho = svm->getDecisionFunction(0, alpha, svidx);

    CV_Assert( alpha.total() == 1 && svidx.total() == 1 && sv_total == 1 );
    CV_Assert( (alpha.type() == CV_64F && alpha.at<double>(0) == 1.) ||
               (alpha.type() == CV_32F && alpha.at<float>(0) == 1.f) );
    CV_Assert( sv.type() == CV_32F );
    hog_detector.clear();

    hog_detector.resize(sv.cols + 1);
	for( int i = 0; i < sv.cols; i++ ) {
// 		cout << sv.at<float>(i) << endl;
        hog_detector[i] = - sv.at<float>(i);
		assert( abs(hog_detector[i]) <= 1 );
	}
	
//     memcpy(&hog_detector[0], sv.ptr(), sv.cols*sizeof(hog_detector[0]));
    hog_detector[sv.cols] = (float)rho;
// 	cout << "rho "<< rho << endl;
}

vector< float > get_svm_detector(const Ptr< ml::SVM >& svm)
{
	vector<float> vec;
	get_svm_detector(svm, vec);
	return vec;
}



// TODO: more efficient version
float calculateJaccard(const Rect& rect1, const Rect& rect2)
{
	// assume rect1, rect2 have the same size (could be used for more efficient calculation)
// 	assert(rect1.height + rect2.height);
// 	assert(rect1.width + rect2.width);
	
	Rect intersect = rect1 & rect2;
	
	// apply formula from https://en.wikipedia.org/wiki/Jaccard_index
	int areaIntersect = intersect.area();
	float numerator = static_cast<float>(areaIntersect);
	float denominator = static_cast<float>(rect1.area() + rect2.area() - areaIntersect);
	
    float jaccard = numerator / denominator;
    return jaccard;
}



void evaluateResults(std::vector< cv::Rect > hits,  Rect trueRect, float jaccardThreshold, uint& truePos, uint& falsePos)
{
	truePos = 0;
	falsePos = 0;
	
	for ( Rect& hit : hits ){
		if ( calculateJaccard(hit, trueRect) >= jaccardThreshold ) {
			truePos++;
		} else {
			falsePos++;
		}
	}
}

void evaluateResults(std::vector< cv::Rect > hits, std::vector< double > confVals, Rect trueRect, float jaccardThreshold, uint& truePos, uint& falsePos, float& jaccardOfMaximum)
{
    if(confVals.empty()) {
        return;

    }
    assert( hits.size() == confVals.size() );
	truePos = 0;
	falsePos = 0;
	uint argMax = 0;
	double max = numeric_limits<double>::min();
	
	for ( uint i = 0; i < hits.size(); i++ ){
		
		if ( confVals[i] > max ) {
			max = confVals[i];
			argMax = i;
		}
		
		if ( calculateJaccard(hits[i], trueRect) >= jaccardThreshold ) {
			truePos++;
		} else {
			falsePos++;
		}
	}
	if ( hits.size() > 0 ) {
		jaccardOfMaximum = calculateJaccard(hits[argMax], trueRect);
	} else {
		jaccardOfMaximum = 0.0f;
	}
}

