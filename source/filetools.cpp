#include "filetools.hpp"

#include <algorithm>
#include <string.h>

using namespace std;

bool isImageByExtension ( boost::filesystem::path file )
{
	string fileextension (file.extension().string());
	transform ( fileextension.begin(), fileextension.end(), fileextension.begin(), ::tolower );

    if( fileextension == ".jpg" ||
            fileextension == ".jpeg" ||
            fileextension == ".png" ||
            fileextension == ".bmp" ) {
        return true;
    } else {
        return false;
    }
}


bool isVideoByExtension( boost::filesystem::path file )
{	
	string fileextension (file.extension().string());
	transform ( fileextension.begin(), fileextension.end(), fileextension.begin(), ::tolower );
    
    if ( fileextension == ".mp4" ||
            fileextension == ".mpeg" ||
            fileextension == ".avi" ||
            fileextension == ".mpg" ) {
        return true;
    } else {
        return false;
    }
}


bool isImageByExtension ( std::string& file )
{
	return isImageByExtension (boost::filesystem::path(file));
}

bool isVideoByExtension ( std::string& file )
{	
	return isVideoByExtension(boost::filesystem::path(file));
}

bool isExistingImageFile(string& file)
{
	return isExistingImageFile(boost::filesystem::path(file));
}

bool isExistingImageFile(boost::filesystem::path file)
{
	return boost::filesystem::is_regular_file(file) && isImageByExtension(file);
}


bool isExistingVideoFile(string& file)
{
	return isExistingVideoFile(boost::filesystem::path(file));
}

bool isExistingVideoFile(boost::filesystem::path file)
{
	return boost::filesystem::is_regular_file(file) && isVideoByExtension(file);
}


